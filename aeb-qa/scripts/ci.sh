#!/bin/bash

if [ -e "/etc/profile.d/rbenv.sh" ]; then
    echo "Loading rbenv..."
    . /etc/profile.d/rbenv.sh
fi

gem install bundler --no-ri --no-rdoc
rbenv rehash # make sure new bundle executable is available
bundle install
TEST_DRIVER=browser_stack TEST_ENV=dev bundle exec rspec spec/features/permissions_spec.rb:8 --options .rspec.ci
