#!/bin/bash

RBVER="`cat .ruby-version`"
RBDIR="/usr/local/rbenv/versions/$RBVER"

if [ -e "/etc/profile.d/rbenv.sh" ]; then
    echo "Loading rbenv..."
    . /etc/profile.d/rbenv.sh
fi

echo "Checking for rbenv version $RBVER in $RBDIR"

if [ -d "$RBDIR" ]; then
    echo "Ruby version already installed"
else
    echo "Need to install ruby version $RBVER"
    rbenv install $RBVER
    exit $?
fi
