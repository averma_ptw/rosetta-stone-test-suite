#!/bin/bash

if [ -e "/etc/profile.d/rbenv.sh" ]; then
    echo "Loading rbenv..."
    . /etc/profile.d/rbenv.sh
fi

gem install bundler --no-ri --no-rdoc
rbenv rehash # make sure new bundle executable is available
bundle install
TEST_DRIVER=browser_stack TEST_ENV=prod bundle exec rspec spec/features/prod_sign_in_spec.rb --options .rspec.ci
