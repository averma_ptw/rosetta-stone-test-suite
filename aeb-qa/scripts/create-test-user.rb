require 'couchrest'
require 'json'

##
# Create a new User and Wallet for the email addres specified on the CouchDB instance specified.
# The user will be created for the default Aria organization and with a password of "password".
# 
# @param scholar_database Dev instance (ex livemocha-dev.cloudant.com/scholar)
# @param email Email address for newly-created user
def create_user couch_username, couch_password, scholar_database, email

  puts "Connecting to https://#{couch_username}:#{couch_password}@#{scholar_database}"

  couch = CouchRest.database("https://#{couch_username}:#{couch_password}@#{scholar_database}")

  stubs_dir = "#{File.dirname(__FILE__)}/stubs"

  puts "Reading mocks from #{stubs_dir}"

  stub_user_json = JSON.parse( IO.read( "#{stubs_dir}/stub_user.json" ) )
  stub_wallet_json = JSON.parse( IO.read( "#{stubs_dir}/stub_wallet.json" ) )

  user_response = couch.save_doc( stub_user_json )

  puts "Created User:#{user_response}:#{user_response['id']}"

  user = couch.get( user_response['id'] )

  puts "Loaded newly-created User:#{user}:#{user['_id']}"

  wallet_response = couch.save_doc( stub_wallet_json )

  puts "Created Wallet:#{wallet_response}:#{wallet_response['id']}"

  wallet = couch.get( wallet_response['id'] )

  puts "Loaded newly-created Wallet:#{wallet}:#{wallet['_id']}"

  wallet['user'] = user['_id']

  puts "Updating Wallet doc with User ID..."

  couch.save_doc( wallet )

  now = Time.now.strftime('%Y-%m-%dT%H:%M:%S+0000')

  puts "Updating User doc with Wallet ID and timestamps..."

  user['email'] = email
  user['wallet'] = wallet['_id']
  user['creationDate'] = now
  user['registrationDate'] = now

  couch.save_doc( user )

  user
end

def get_parameter(arg_index, default_value)
  if ARGV.length >= arg_index + 1
    return ARGV[arg_index]
  else
    return default_value
  end
end

couch_username = get_parameter 0, 'rosetta-aeb-automated'
couch_password = get_parameter 1, 'SmgdEcZZ'
scholar_database = get_parameter 2, 'livemocha-dev.cloudant.com/scholar'
email = get_parameter 3, 'test_brian_3@rosettastone.com'

puts "creating user with\ncouch_username: #{couch_username}\ncouch_password: #{couch_password}\nscholar_database: #{scholar_database}\nemail: #{email}"
create_user(couch_username, couch_password, scholar_database, email)