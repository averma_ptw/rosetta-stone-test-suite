require 'capybara/rspec'
require 'capybara/poltergeist'
require 'selenium-webdriver'
require 'selenium-helper'
require 'seleniumhelper/browserstack'
require 'seleniumhelper/capybara_selector_overrides'
Dir[File.expand_path('../support/**/*.rb', __FILE__)].each {|f| require f }

RSpec.configure do |config|
  [AebHelpers, SeleniumHelper::StandardLib].each do |module_to_include|
    config.include(module_to_include)
  end
end
