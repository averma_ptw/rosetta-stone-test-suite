module AebHelpers

  # These hash store the test lesson and answer details
  TEST_LESSON = {goal_name: "PTW test goal", lesson1_name: "Test lesson with video in introduction", lesson2_name: "Usage Practice Testing 2", lesson3_name: "Usage Practice Testing 3", lesson4_name: "Lesson with a Test activity"}

  TEST_LESSON2 = {goal_name: "2906 Test 3", lesson1_name: "extended speaking and writing"}

  TEST_LESSON3 = {goal_name: "George", lesson1_name: "Om "}

  TEST_ANSWER_DETAILS = {"Choose the correct answer from the choices below." => "1",
                         "Choose the correct answer for each blank. Use what you learned in the lesson." => ["drop", "down"],
                         "Type the correct answer for each blank. Use what you learned in the lesson." => "blank",
                         "Highlight the 2 errors that you find in the text." => ["ferror", "them"]}

  TEST_QUESTION_DETAILS = {"Choose the correct answer from the choices below." => :multiple_choices,
                           "Type the correct answer for each blank. Use what you learned in the lesson." => :fillintheblank,
                           "Choose the correct answer for each blank. Use what you learned in the lesson." => :fillin_dropdown,
                           "Highlight the 2 errors that you find in the text." => :find_error}

  WRONG_ANSWER_DETAILS = {"Choose the correct answer from the choices below." => "2",
                          "Type the correct answer for each blank. Use what you learned in the lesson." => "plank",
                          "Choose the correct answer for each blank. Use what you learned in the lesson." => ["crop", "done"],
                          "Highlight the 2 errors that you find in the text." => ["Find", "keep"]}

  TEST_QUESTION_DETAILS_FOR_MY_GOALS = {"Type the correct answer for each blank. Use what you learned in the lesson." => :fillintheblank,
                                        "Choose the correct answer for each blank. Use what you learned in the lesson." => :fillin_dropdown,
                                        "Highlight the 1 error that you find in the text." => :find_error}

  TEST_ANSWER_DETAILS_FOR_MY_GOALS = {"Choose the correct answer for each blank. Use what you learned in the lesson." => "blank",
                                      "Type the correct answer for each blank. Use what you learned in the lesson." => "blank",
                                      "Highlight the 1 error that you find in the text." => ["first"]}

  TEST_QUESTIONS_FOR_MY_GOALS = {"Type the correct answer for each blank. Use what you learned in the lesson.Justfillinthe" => :fillintheblank,
                                 "Type the correct answer for each blank. Use what you learned in the lesson.Fillinthe" => :twofillintheblanks,
                                 "Choose the correct answer for each blank. Use what you learned in the lesson.Justfillinthethe" => :fillin_dropdown,
                                 "Choose the correct answer for each blank. Use what you learned in the lesson.Fillinthe" => :twofillin_dropdowns,
                                 "Choose the correct answer from the choices below.Just pick 1" => :multiple_choices,
                                 "Highlight the 1 error that you find in the text.Lastoneisthefirst" => :find_error}

  TEST_ANSWERS_FOR_MY_GOALS = {"Type the correct answer for each blank. Use what you learned in the lesson.Justfillinthe" => ["blank"],
                               "Type the correct answer for each blank. Use what you learned in the lesson.Fillinthe" => ["water", "pond"],
                               "Choose the correct answer for each blank. Use what you learned in the lesson.Justfillinthethe" => ["blank"],
                               "Choose the correct answer for each blank. Use what you learned in the lesson.Fillinthe" => ["another", "blank"],
                               "Choose the correct answer from the choices below.Just pick 1" => ["1"],
                               "Highlight the 1 error that you find in the text.Lastoneisthefirst" => ["first"]}

  TERMS_OF_USE_URL = "http://resources.rosettastone.com/CDN/us/agreements/Combined-Online-Terms-of-Use-100113-1.pdf"

  PRIVACY_POLICY_URL = "http://resources.rosettastone.com/CDN/global/agreements/Online_Interactive_Product_Privacy_Policy_52814-1.pdf"

  SUPPORT_PAGE_URL = "http://support.rosettastone.com/en/Business/"

  CHECK_TABS = {my_goals: 'My goals', add_new_goals: 'Add new goals', notifications: 'Notifications', my_profile: 'My profile', support: 'Support', sign_out: 'Sign out'}

  PLACEMENT_TEST_ANSWERS = {form_a_correct_answers: ["quite", "finally", "factory", "a/the", "in", "very or more than usual", "in a short time",
                                                     "for all time in the future", "aren’t", "stay", "to be easy to see or notice", "to fill an amount of space or time",
                                                     "to refuse an offer or request", "had been", "unless", "to think of an idea or plan", "to strongly dislike someone or something",
                                                     "to think carefully about something", "have I met", "would have gone"],

                            form_a_wrong_answers: ["always", "usually", "transportation", "a/an", "at", "in a short time", "for all time in the future",
                                                   "happening earlier than expected", "wouldn’t", "stayed", "to fill an amount of space or time", "to refuse an offer or request",
                                                   "to defend or support a particular idea", "has been", "if only", "to think carefully about something", "to think of an idea or plan",
                                                   "to make something stronger", "I had met", "would go"],

                            form_a_below_b1_answers: [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

                            form_a_c1_answers: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],

                            form_a_to_form_h_answers: [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

                            form_h_correct_answers: ["intend", "solutions", "much more", "Though", "facts related to the topic", "to make people curious",
                                                     "to be involved or participating", ["trainer", "She likes helping people to be successful", "take the new position"],
                                                     "recommendation", "ignore", "reach", "had had", "Be Your Own Person", "Do not follow your boss’s ideas blindly.",
                                                     "Help find solutions to problems.", ["She has a lot to do before guests arrive.", "entertain some important guests", "He wants to receive the hotel information this way."]],

                            form_h_wrong_answers: ["suggest", "methods", "all the more ", "So", "general trends in the market", "to find out if people are listening",
                                                   "to be pleasant or interesting", ["career advisor", "She respects her boss", "change companies"],
                                                   "information", "recall", "will reach", "have", "How to Please the Boss", "Do what your boss asks you to.",
                                                   "Agree with your colleagues’ ideas.", ["She can't get a hotel reservation.", "make a hotel reservation", "He is confused by the woman's message."]],

                            form_h_b1_answers: [1, 1, 1, 1, 1, 0, 0, [0, 0, 0], 0, 0, 0, 0, 0, 0, 0, [0, 0, 0]],

                            form_h_b2_answers: [1, 1, 1, 1, 1, 1, 1, [1, 1, 1], 1, 1, 1, 1, 1, 1, 1, [1, 0, 0]]
  }

  COLUMN_VALUES = {"Authoring" => "Draft",
                   "Pending Review" => "Pending",
                   "In Review" => "InReview",
                   "Approved" => "Approved",
                   "Published" => "Published",
                   "Cleanup" => "Cleanup",
                   "Me" => "Test U"}

  COLUMN_POSITION = {"Language" => "3",
                     "Level" => "4",
                     "State" => "5",
                     "Author" => "2"}

  def login_as_user_and_go_to_aeb(*args)
    if args.first.is_a?(Hash)
      options = args.first
      username = options[:username]
      password = options[:password]
    else
      username, password = args
    end
    go_to_launchpad
    login(:username => username, :password => password)
    @wait.until { launch_pad_elements(:aria_button) }
    launch_pad_elements(:aria_button).click
  end

  def go_to_launchpad
    @driver ||= page.driver.browser # setting up @driver is a requirement when using selenium-helper currently
    page.driver.browser.manage.window.maximize
    @wait ||= Selenium::WebDriver::Wait.new(:timeout => SeleniumHelper::Settings::DEFAULT_WAIT)
    launchpad_url = SeleniumHelper::Settings.launchpad_url
    @driver.get launchpad_url
  end

  # you should never have to slow down if the site elements are actually ready to be interacted with, even
  # if customers generally do not interact with them immediately. but there might actually be bugs on the site
  # that we need to work around.
  def chill_out_for_a_bit_to_accommodate_site_bug
    sleep(0.5)
  end

  # assumes localization language starts out as English and gets set back to English
  def assert_localization_works
    wait_for_loaded

    click_on 'My profile'

    within('.learner-settings-view') do
      click_on 'Edit'
    end

    change_localization_language_to('Deutsch')
    expect(page).to have_content 'Anweisungen anzeigen auf'

    within('.learner-settings-view') do
      click_on 'Bearbeiten'
    end

    change_localization_language_to('English')
    expect(page).to have_content 'Show instructions in'
  end

  def change_localization_language_to(display_name)
    first('select[ng-model="editableUser.localizationLanguage"]').find(:option, display_name).select_option

    # when you're changing your localization language, i'm thinking there isn't enough time in here for the change to fully "take" in sproutcore.
    # it "takes" enough to allow the page to reload, but doesn't actually change the locale, at times, it seems. bad sproutcore code in here somewhere.
    #chill_out_for_a_bit_to_accommodate_site_bug

    find('button[l10n=_save]').click
  end

  def feedback_request_submission_selector
    '.feedback-request-item'
  end

  def wait_for_loaded
    expect(page).to_not have_css('#loading')
  end

  def create_user(options = {})
    creation_options = {:register => true, :roles => ["role.AriaLearner", "role.AriaTutoringStudent"], :level => 'B1'}.merge(options)
    create_proto_user(creation_options)
  end

  def expect_vocab_card_to_be_present
    expect(page).to have_css '.card-front'
  end

  def select_goal_and_lesson(goal_title, lesson_title)
    expect(page).to have_css '.goal-title-column'

    if page.has_no_content?(lesson_title)
      click_on goal_title
      click_on lesson_title
    else
      click_on lesson_title
    end
  end

  def restart_activity_after_answering(number_of_answers)
    for i in 0..number_of_answers #This loop will answer a part of questions
      expect(page).to have_css '.activity-button'
      click_button 'Check My Answer'
      click_button 'Skip'
    end
    expect(page).to have_css '.progress-indicator.incorrect'
    click_button 'Restart the activity' #Clicking Restart button in the middle of answering questions

    expect(page).to have_css '.activity-button'
    expect(page).to have_css '.progress-indicator.inProgress'
    expect(page).to_not have_css '.progress-indicator.incorrect'
    expect(page).to_not have_css '.progress-indicator.correct'
  end

  def answering_activity_either_fully_correct_or_fully_wrong(test_questions_list, answers_list, number_of_questions: test_questions_list.size)

    for i in 1..number_of_questions #This loop will select either correct or incorrect answers for the questions according to the parameters given
      expect(page).to have_css '.activity-button'
      question_type = find(:xpath, "//div[@class='ng-binding']").text

      if (test_questions_list[question_type] == :multiple_choices)
        click_button answers_list[question_type]

      elsif (test_questions_list[question_type] == :fillintheblank)
        @driver.find_element(:xpath, "//input").send_keys answers_list[question_type]

      elsif (test_questions_list[question_type] == :fillin_dropdown)
        number_of_select_tags = page.all(:xpath, "(//select[contains(@class,'dropdown')])").length
        if number_of_select_tags == 1
          @driver.find_element(:xpath, "(//select[contains(@class,'dropdown')])[1]//option[text()='#{answers_list[question_type]}']").click
        else
          find(:xpath, "(//select[contains(@class,'dropdown')])[1]//option[text()='#{answers_list[question_type][0]}']").click
          find(:xpath, "(//select[contains(@class,'dropdown')])[2]//option[text()='#{answers_list[question_type][1]}']").click
        end

      elsif (test_questions_list[question_type] == :find_error)
        answers_list[question_type].each do |answer|
          @driver.find_element(:xpath, "//span[text()='#{answer}']").click
        end
      end
      #click_on 'Check My Answer'
      find(:xpath, "//button[text()='Check My Answer']").click
      expect(page).to have_xpath "//div[contains(@class,'progress-indicator-bar')]//li[#{i}][contains(@class,'correct')]" #This checks if the progress bar is filled as the the questions are answered
      if page.has_content?('Skip')
        click_button 'Skip'
      end
      expect(page).to_not have_content question_type
    end
  end

  def finish_activity_with_half_answers_correct(test_questions_list, correct_answer_list, wrong_answer_list)
    answer_correctly = 0
    for i in 1..AebHelpers::TEST_ANSWER_DETAILS.size #This is loop is to select partially correct answer for all questions
      answer_correctly = (answer_correctly + 1) % 2
      expect(page).to have_css '.activity-button'
      question_type = find(:xpath, "//div[@class='ng-binding']").text

      if (test_questions_list[question_type] == :multiple_choices)
        if answer_correctly == 1
          click_button correct_answer_list[question_type]
        else
          click_button wrong_answer_list[question_type]
        end

      elsif (test_questions_list[question_type] == :fillintheblank)
        if answer_correctly == 1
          find(:xpath, "//input").set correct_answer_list[question_type]
        else
          find(:xpath, "//input").set wrong_answer_list[question_type]
        end

      elsif (test_questions_list[question_type] == :fillin_dropdown)
        if answer_correctly == 1
          find(:xpath, "(//select[contains(@class,'dropdown')])[1]//option[text()='#{correct_answer_list[question_type][0]}']").click
          find(:xpath, "(//select[contains(@class,'dropdown')])[2]//option[text()='#{correct_answer_list[question_type][1]}']").click
        else
          find(:xpath, "(//select[contains(@class,'dropdown')])[1]//option[text()='#{wrong_answer_list[question_type][0]}']").click
          find(:xpath, "(//select[contains(@class,'dropdown')])[2]//option[text()='#{wrong_answer_list[question_type][1]}']").click
        end

      elsif (test_questions_list[question_type] == :find_error)
        if answer_correctly == 1
          find(:xpath, "//span[text()='#{correct_answer_list[question_type][0]}']").click
          find(:xpath, "//span[text()='#{correct_answer_list[question_type][1]}']").click
        else
          find(:xpath, "//span[text()='#{wrong_answer_list[question_type][0]}']").click
          find(:xpath, "//span[text()='#{wrong_answer_list[question_type][1]}']").click
        end
      end
      click_button 'Check My Answer'
      expect(page).to have_xpath("//div[contains(@class,'progress-indicator-bar')]//li[#{i}][contains(@class,'correct')]")
      if page.has_content?('Skip')
        click_button 'Skip'
      end
      expect(page).to_not have_content question_type
    end
  end

  def create_user_and_register_details #creates new user and register the details
    user = create_proto_user()
    email = user[:email]
    id = user[:_id]

    @driver ||= page.driver.browser # setting up @driver is a requirement when using selenium-helper currently
    @wait ||= Selenium::WebDriver::Wait.new(:timeout => SeleniumHelper::Settings::DEFAULT_WAIT)
    register_url = SeleniumHelper::Settings.launchpad_url + "#/register/" + id # this is the registration url sent through email
    @driver.get register_url

    # Filling in registration details and click on Agree and Continue
    expect(page).to have_content email
    fill_in 'user-password', :with => SeleniumHelper::Settings.environment_settings["valid_password"]
    fill_in 'user-confirmPassword', :with => SeleniumHelper::Settings.environment_settings["valid_password"]
    select "English (American)", :from => 'user-originLanguage'
    select "Adult female", :from => 'user-voiceType'
    select "America/Chicago", :from => 'user-timezone'
    page.check('user-is-active')

    click_on('Agree and Continue')

    @wait.until { launch_pad_elements(:aria_button) }
    launch_pad_elements(:aria_button).click
    return user
  end

  def select_level_for_new_user(cefr_level)

    #Self placing into given level
    find(:xpath, "//span[text()='I’ll determine my own language ability.']").click
    find(:xpath, "//button[@ng-click=\"pickLevel('#{cefr_level}')\"]/span").click
    wait_for_loaded
    if cefr_level.eql? "BELOW_B1"
      click_on("Continue to Goals")
      cefr_level = "B1"
    end
    expect(page).to have_content cefr_level
  end

  def run_placement_test(correct_answers, wrong_answers, answer_correctly)

    number_of_questions = answer_correctly.size

    for i in 0..(number_of_questions - 1)

      if (page.has_xpath?(".//*[@class='jp-audio']")) # this is a listening exercise with multiple questions

        correct_answers[i].zip(answer_correctly[i]).each do |answer, choose_correct_answer|
          if choose_correct_answer == 1 # choose correct answers
            if answer.eql?('trainer') # unique exception
              find(:xpath, "//div[@id='AnswerPanel']/p//span[text()=\"#{answer}\"]//preceding-sibling::input").click
            else
              find(:xpath, "//div[@id='AnswerPanel']/p//span[text()=\"#{answer}\"]//preceding-sibling::strong//span//input").click
            end
          end
        end

        wrong_answers[i].zip(answer_correctly[i]).each do |answer, choose_correct_answer|
          if choose_correct_answer == 0 # choose wrong answers
            if answer.eql?('career advisor') # # unique exception
              find(:xpath, "//div[@id='AnswerPanel']/p//span[text()=\"#{answer}\"]//preceding-sibling::input").click
            else
              find(:xpath, "//div[@id='AnswerPanel']/p//span[text()=\"#{answer}\"]//preceding-sibling::strong//span//input").click
            end
          end
        end

      else # page has single question
        if answer_correctly[i] == 1 # choose correct answer
          find(:xpath, "//*[@id='AnswerPanel']//tr[td//span[text()=\"#{correct_answers[i]}\"]]//input").click
        else # choose wrong answer
          find(:xpath, "//*[@id='AnswerPanel']//tr[td//span[text()=\"#{wrong_answers[i]}\"]]//input").click
        end
      end

      click_on('Next')
    end
  end

  def take_placement_test(cefr_level)

    # Take placement test
    click_on "Take Placement Test"
    click_on "Next" # placement test message

    case(cefr_level)
      when "BELOW_B1"
        run_placement_test(PLACEMENT_TEST_ANSWERS[:form_a_correct_answers], PLACEMENT_TEST_ANSWERS[:form_a_wrong_answers], PLACEMENT_TEST_ANSWERS[:form_a_below_b1_answers])
        click_on("Continue to Goals") # below B1 difficulty warning
        cefr_level = "B1"
      when "C1"
        run_placement_test(PLACEMENT_TEST_ANSWERS[:form_a_correct_answers], PLACEMENT_TEST_ANSWERS[:form_a_wrong_answers], PLACEMENT_TEST_ANSWERS[:form_a_c1_answers])
      when "B1"
        run_placement_test(PLACEMENT_TEST_ANSWERS[:form_a_correct_answers], PLACEMENT_TEST_ANSWERS[:form_a_wrong_answers], PLACEMENT_TEST_ANSWERS[:form_a_to_form_h_answers])
        run_placement_test(PLACEMENT_TEST_ANSWERS[:form_h_correct_answers], PLACEMENT_TEST_ANSWERS[:form_h_wrong_answers], PLACEMENT_TEST_ANSWERS[:form_h_b1_answers])
      when "B2"
        run_placement_test(PLACEMENT_TEST_ANSWERS[:form_a_correct_answers], PLACEMENT_TEST_ANSWERS[:form_a_wrong_answers], PLACEMENT_TEST_ANSWERS[:form_a_to_form_h_answers])
        run_placement_test(PLACEMENT_TEST_ANSWERS[:form_h_correct_answers], PLACEMENT_TEST_ANSWERS[:form_h_wrong_answers], PLACEMENT_TEST_ANSWERS[:form_h_b2_answers])
    end

    expect(page).to have_content cefr_level
  end

  def select_first_three_goals_for_new_user
    find(:xpath, "//div[@class='goal-selection-list']//li[1]").click
    find(:xpath, "//div[@class='goal-selection-list']//li[2]").click
    find(:xpath, "//div[@class='goal-selection-list']//li[3]").click
    click_on("Continue")
  end

  def check_values_in_cefr_level_column(*args) #checks the level column or type column or both column which is selected
    all_values_in_column = page.all('.level-name').map { |text| text.text }
    all_values_in_column.each do |each_value_in_column|
      expect(args).to have_content each_value_in_column
    end
  end

  def check_values_in_practice_type_column(*args) #checks the level column or type column or both column which is selected
    all_values_in_column = page.all('.mode-name').map { |text| text.text }
    all_values_in_column.each do |each_value_in_column|
      expect(args).to have_content each_value_in_column
    end
  end

  def choose_cefr_level(cefr_level) #choose the cefr level from select dropdown
    find(:xpath, "//select[@ng-model='options.level']//option[text()='#{cefr_level}']").click
    wait_for_loaded
  end

  def choose_practice_types(practice_type) #choose the practice type from select dropdown
    find(:xpath, "//select[@ng-model='options.mode']//option[text()='#{practice_type}']").click
    wait_for_loaded
  end

  def click_on_any_review #click on any review
    find(:xpath, "(//a[@class='feedback-request-item ng-scope'])[3]//div[1]").click
    wait_for_loaded
  end

  def review_five_feedback_requests #click on five different reviews and then click on back button in browser
    for i in 1..5
      # Verifying username in the feedback report
      full_user_name = find(:xpath, "(//a[@class='feedback-request-item ng-scope'])[3+#{i}]//div[1]").text
      split_user_name = full_user_name.split(" ")
      user_first_name = split_user_name.first
      user_initial = split_user_name[1].chars.first

      # Click on next review
      find(:xpath, "(//a[@class='feedback-request-item ng-scope'])[3+#{i}]//div[1]").click
      wait_for_loaded
      expect(page.text).to match(/Leave\s#{user_first_name}.+\san\sexpert\sreview/) #to make sure the review is same is as the one we clicked on
      page.evaluate_script('window.history.back()')
      wait_for_loaded
    end
  end

  def verify_buttons_try_again_and(sbutton)
    expect(page).to have_content 'Try Again'
    if (sbutton).eql? 'Skip'
      expect(page).to have_content 'Skip'
    elsif (sbutton).eql? 'Next Activity'
      expect(page).to have_content 'Next Activity'
    end
  end

  def check_for_next_activity_button(activity_name)
    expect(page).to have_xpath "//a[@class='nav-item ng-scope disabled'][span[text()='#{activity_name}']]"
  end

  def create_user_and_select_goals(*args) # creates new user and select three goals including the user specified optional goal
    user = create_user(:how_level_was_chosen => 'self')
    email = user[:email]
    password = SeleniumHelper::Settings.environment_settings["valid_password"]
    login_as_user_and_go_to_aeb(email, password)
    expect(page).to have_css '.choose-goals-view'
    if (args.length==0)
      select_first_three_goals_for_new_user
    else
      list_of_goals = page.all(:xpath, "//div[@class='goal-selection-list']//li").map { |each_goal| each_goal.text }
      index_of_goal = list_of_goals.index(args[0])
      find(:xpath, "//div[@class='goal-selection-list']//li[#{index_of_goal + 1}]").click
      find(:xpath, "//div[@class='goal-selection-list']//li[#{((index_of_goal + 1) % list_of_goals.size) + 1}]").click
      find(:xpath, "//div[@class='goal-selection-list']//li[#{((index_of_goal + 2) % list_of_goals.size) + 1}]").click
      click_on "Continue"
    end
  end

  def check_end_of_activity_and_click_on (activity)
    expect(page).to have_css(".correct-count")
    expect(page).to have_xpath("//div[text()='Your last attempt']/preceding-sibling::div[@class='correct-count ng-binding']")

    verify_buttons_try_again_and("Next Activity")
    click_button activity

    if (activity).eql? 'Next Activity'
      expect(page).to have_xpath '//a[span[text()=\'Usage practice\']]/div[@class=\'progress-bar ng-scope\']'
      click_on 'Usage practice'
    end
  end

  def check_help_button_functionality
    find(:xpath, "//button[@class='btn-help']").click #Clicking on Help button to check if pop-up appears
    expect(page).to have_css '.help-popover'

    find(:xpath, "//button[@class='btn-help selected']").click #Clicking on Help button again to check if pop-up disappears
    expect(page).to_not have_css '.help-popover'

    find(:xpath, "//button[@class='btn-help']").click #Clicking on Help button to check if pop-up appears
    expect(page).to have_css '.help-popover'

    find(:css, ".ng-scope>body").click #Clicking somewhere outside the Help pop-over to check if pop-up disappears
    expect(page).to_not have_css '.help-popover'
  end

  def check_flag_content_and_click_on (button)

    find(:xpath, "//button[@class='flag-content-button']").click #Clicking on flag content to check if pop-up appears and click on Cancel to check if it disappears
    expect(page).to have_content 'Report a problem'
    number_of_radio_buttons = page.all(:xpath, '(//div[@class=\'flag-content-reason ng-scope\']//input[@type=\'radio\'])').length #Fetching radio button contents for future use

    case button
      when 'Cancel'
        click_button button
      when 'close-button'
        find(:xpath, "//button[@class='close-button']").click
    end
    expect(page).to_not have_content 'Report a problem'

    return number_of_radio_buttons
  end

  def report_problem_for_radio_button (index)
    find(:xpath, "//button[@class='flag-content-button']").click
    expect(page).to have_content 'Report a problem'

    find(:xpath, "(//div[@class='flag-content-reason ng-scope']//input[@type='radio'])[#{index}]").click
    if page.has_xpath?("//div[@class='input-wrapper ng-scope']//textarea") # Filling text area for reporting, when required
      find(:xpath, "//div[@class='input-wrapper ng-scope']//textarea").set 'test'
    end

    click_button 'Report Problem'
    expect(page).to_not have_content 'Report a problem'
  end

  def create_user_and_self_place_at(cefr_level)
    create_user_and_register_details
    select_level_for_new_user(cefr_level)
    select_first_three_goals_for_new_user

    expect(page).to have_content 'My goals'
  end

  def create_user_and_take_placement_test(cefr_level)
    create_user_and_register_details
    take_placement_test(cefr_level)
    select_first_three_goals_for_new_user

    expect(page).to have_content 'My goals'
    signout
  end

  def signout_from_admin_tools
    find(:xpath, "//span[@class='home-menu-name ng-binding']").click
    find(:xpath, "//li[@id='signout']").click

    assert_signed_out_from_launchpad
  end

  def verify_color_code(locator, colorattribute, colorvalue) #verifies the color code for a given element
    #extracts the color code in rgba and place it in one-dimensioanl array
    rgba_extractor = page.find(:xpath, locator).native.css_value("#{colorattribute}").scan(/rgba\((\d+),\ (\d+),\ (\d+),\ (\d+)\)/).flatten
    #creates object for Color class and initializing the color codes
    color = Selenium::WebDriver::Support::Color.new(rgba_extractor[0], rgba_extractor[1], rgba_extractor[2], rgba_extractor[3])
    expect(color.hex).to eq colorvalue
  end

  def answer_test_activity(test_question_list, correct_answer_list, number_of_questions: test_question_list.size) # select the answers for the activity "Test"

    for i in 1..number_of_questions

      expect(page).to have_css '.activity-button.ng-scope.ng-isolate-scope'
      question_type = find(:xpath, "//div[@class='ng-binding']").text
      if page.has_css? '.question-text'
        question = page.all(:xpath, "//div[contains(@class,'question-text')]//span[@class='ng-scope ng-binding']").map { |question| question.text }
      else
        question = page.all(:xpath, "//li[@class='prompt text-prompt']//div[@class='ng-scope']").map { |question| question.text }
      end
      sQuestion = ""
      question.each do |question|
        sQuestion = sQuestion + question
      end
      question_type = question_type + sQuestion

      if (test_question_list[question_type] == :multiple_choices)
        for i in 1..correct_answer_list[question_type].size
          click_button correct_answer_list[question_type][i-1]
        end


      elsif (test_question_list[question_type] == :fillintheblank)
        for i in 1..correct_answer_list[question_type].size
          find(:xpath, "//input").set correct_answer_list[question_type][i-1]
        end

      elsif (test_question_list[question_type] == :twofillintheblanks)
        for i in 1..correct_answer_list[question_type].size
          find(:xpath, "(//input)[#{i}]").set correct_answer_list[question_type][i-1]
        end

      elsif (test_question_list[question_type] == :fillin_dropdown)
        for i in 1..correct_answer_list[question_type].size
          find(:xpath, "(//select[contains(@class,'dropdown')])[#{i}]//option[text()='#{correct_answer_list[question_type][i-1]}']").click
        end
      elsif (test_question_list[question_type] == :twofillin_dropdowns)
        for i in 1..correct_answer_list[question_type].size
          find(:xpath, "(//select[contains(@class,'dropdown')])[#{i}]//option[text()='#{correct_answer_list[question_type][i-1]}']").click
        end
      elsif (test_question_list[question_type] == :find_error)
        for i in 1..correct_answer_list[question_type].size
          find(:xpath, "//span[text()='#{correct_answer_list[question_type][i-1]}']").click
        end
      end
      click_on "Submit"
      expect(page).to have_xpath("//div[contains(@class,'progress-indicator-bar')]//li[#{i}][contains(@class,'correct')]") #This checks if the progress bar is filled as the the questions are answered
      expect(page).to_not have_content question_type
    end
    expect(page).to have_content "Test results"
  end

  def verify_user_is_on_profile_tab
    expect(page).to have_css ".user-profile-view"
  end

  def verify_inactive_save_button
    expect(page).to have_xpath "//button[@disabled='disabled' and text()='Save']"
  end

  def verify_active_save_button
    expect(page).to_not have_xpath "//button[@disabled='disabled' and text()='Save']"
  end

  def click_edit_on_password_section
    within(".password-view") { click_button "Edit" }
  end

  def click_inside_password_section(button_name) #clicking (Cancel or Save) arguments inside password section
    within(".password-edit") { click_button button_name }
  end

  def click_edit_on_cefr_level_section
    within(".cefr-level-section") { click_button "Edit" }
  end

  def click_edit_on_learner_settings_section
    within(".learner-settings-view") { find("button[l10n='_edit_button']").click }
  end

  def verify_password_section_expanded
    expect(page).to have_css ".password-edit"
  end

  def verify_password_section_collapsed
    expect(page).to_not have_css ".password-edit"
  end

  def verify_cefr_level_section_expanded
    expect(page).to have_css '.cefr-level-edit'
  end

  def verify_cefr_level_section_collpased
    expect(page).to_not have_css '.cefr-level-edit'
  end

  def verify_learner_settings_section_expanded
    expect(page).to have_css ".learner-settings-edit"
  end

  def verify_learner_settings_section_collapsed
    expect(page).to_not have_css ".learner-settings-edit"
  end

  def click_save_inside_learner_settings_section
    within(".learner-settings-edit") { find("button[l10n='_save']").click }
  end

  def click_cancel_inside_learner_settings_section
    within(".learner-settings-edit") { click_button "Cancel" }
  end

  def fill_in_passwords(*args) #filling up the passwords in old password, new password and confirm password fields
    current_password = args[0]
    new_password = args[1]
    find(:css, "input[name='oldPassword']").set(current_password)
    find(:css, "input[name='newPassword']").set(new_password)
    find(:css, "input[name='confirmNewPassword']").set(new_password)
  end

  def choose_language_and_time_zone(number_of_selections, action_button) #select the instruction language, native language,time zone

    if number_of_selections != 1
      native_language = find(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select").find('option[selected]').text
      selected_option_for_native_language = find(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select//option[@selected='selected']")[:value].to_i
      number_of_options_for_native_language = page.all(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select//option").size.to_i
      selected_native_language = find(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select//option[@value='#{(selected_option_for_native_language + 1) % number_of_options_for_native_language}']").text
      find(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select//option[@value='#{(selected_option_for_native_language + 1) % number_of_options_for_native_language}']").click
      time_zone = find(:xpath, "//div[@l10n='_edit_profile_time_zone']/following-sibling::select").find('option[selected]').text
      selected_option_for_time_zone = find(:xpath, "(//div[@l10n='_edit_profile_time_zone'])[2]//following-sibling::select//option[@selected='selected']")[:value].to_i
      number_of_options_for_time_zone = page.all(:xpath, "//div[@l10n='_edit_profile_time_zone']/following-sibling::select//option").size.to_i
      selected_time_zone = find(:xpath, "//div[@l10n='_edit_profile_time_zone']/following-sibling::select//option[@value='#{(selected_option_for_time_zone + 1) % number_of_options_for_time_zone}']").text
      find(:xpath, "//div[@l10n='_edit_profile_time_zone']/following-sibling::select//option[@value='#{(selected_option_for_time_zone + 1) % number_of_options_for_time_zone}']").click
    end

    if number_of_selections != 2
      instruction_language = find(:xpath, "//div[@l10n='_edit_profile_site_display_language']/following-sibling::select//option[@selected='selected']").text
      selected_option_for_instruction_language = find(:xpath, "(//div[@l10n='_edit_profile_site_display_language'])[2]/following-sibling::select//option[@selected='selected']")[:value].to_i
      number_of_options_for_instruction_language = page.all(:xpath, "//div[@l10n='_edit_profile_site_display_language']/following-sibling::select//option").size.to_i
      selected_instruction_language = find(:xpath, "//div[@l10n='_edit_profile_site_display_language']/following-sibling::select//option[@value='#{(selected_option_for_instruction_language + 1) % number_of_options_for_instruction_language}']").text
      find(:xpath, "//div[@l10n='_edit_profile_site_display_language']/following-sibling::select//option[@value='#{(selected_option_for_instruction_language + 1) % number_of_options_for_instruction_language}']").click
    end

    selected_values = [native_language, time_zone, instruction_language] if number_of_selections == 3
    selected_values = [selected_native_language, selected_time_zone] if number_of_selections == 2
    selected_values = [selected_instruction_language] if number_of_selections == 1

    case action_button
      when :cancel
        click_cancel_inside_learner_settings_section
        verify_learner_settings_section_collapsed

      when :save
        click_save_inside_learner_settings_section
        wait_for_loaded
        verify_learner_settings_section_collapsed

      when :edit
        click_edit_on_cefr_level_section
    end

    if !action_button.eql?(:edit)
      verify_selected_options_displayed_on_learner_settings_section(selected_values)
    end
  end

  #verify the selected options on learner section
  def verify_selected_options_displayed_on_learner_settings_section(*args)
    args.flatten.each do |each_selected_value|
      expect(page).to have_xpath "//div[text()='#{each_selected_value}']"
    end
  end

  #changes the lesson level and verify goals are present for the level selected
  def change_lesson_level_and_verify_goals_are_present_in_cefr_levels
    within(".cefr-level-section") { click_button "Edit" }
    expect(page).to have_css '.cefr-level-edit'
    cefr_level = find(:xpath, "//td[button[@class='cefr-radio-button selected']]//following-sibling::td[@ng-bind='cefrLevel.value']").text
    if cefr_level.eql? 'B1'
      find(:xpath, "//table[@class='cefr-level-choices']//tr[td[text()='B2']]//button").click
    elsif cefr_level.eql? 'B2'
      find(:xpath, "//table[@class='cefr-level-choices']//tr[td[text()='C1']]//button").click
    else
      find(:xpath, "//table[@class='cefr-level-choices']//tr[td[text()='B1']]//button").click
    end
    within(".cefr-level-edit") { click_button "Save" }
    wait_for_loaded
    popup_status = 0
    if page.has_css?(".modal-dialog")
      within(".modal-dialog") do
        expect(page).to have_content "Would you like to add some goals at your new lesson level?"
        find("button[ng-if='!!cancelLabel'] > span").click
      end
      popup_status += 1
    end
    expect(page).to_not have_css ".modal-dialog"
    verify_cefr_level_section_collpased

    click_on "My goals"
    expect(page).to have_css ".my-goals-view"

    if popup_status == 0
      expect(page).to have_css ".goal-title-column.ng-binding.level-match"
    else
      expect(page).to_not have_css ".goal-title-column.ng-binding.level-match"
    end
    click_on "My profile"
    verify_user_is_on_profile_tab
  end

  def reset_learner_settings
    selected_voice_type = find(:xpath, "(//div[@l10n='_edit_profile_voice_type'])[2]//following-sibling::select//option[1]").text
    find(:xpath, "(//div[@l10n='_edit_profile_voice_type'])[2]//following-sibling::select//option[1]").click
    selected_time_zone = find(:xpath, "(//div[@l10n='_edit_profile_time_zone'])[2]//following-sibling::select//option[11]").text
    find(:xpath, "(//div[@l10n='_edit_profile_time_zone'])[2]//following-sibling::select//option[11]").click
    selected_native_language = find(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select//option[18]").text
    find(:xpath, "//div[@l10n='_edit_profile_native_language']/following-sibling::select//option[18]").click
    return [selected_voice_type, selected_time_zone, selected_native_language]
  end

  def login_as_user_and_go_to_editor(*args)
    if args.first.is_a?(Hash)
      options = args.first
      username = options[:username]
      password = options[:password]
    else
      username, password = args
    end
    go_to_launchpad
    login(:username => username, :password => password)
    @wait.until { @driver.find_element(:id => 'launch_editor') }
    @driver.find_element(:id => 'launch_editor').click
  end

  def check_new_lesson_objectives
    expect(page).to have_xpath "//textarea[@placeholder='Objective 1']"
    number_of_objectives = page.all(:xpath, "//textarea[contains(@placeholder,'(optional)')]").size
    expect(number_of_objectives).to eq 4
    expect(page).to have_xpath "//button[@ng-click='save()' and @disabled='']"
    return number_of_objectives
  end

  def create_a_writing_practice_activity
    click_on "Writing Practice"
    click_button "Add Question"

    find(:xpath, "(//label[text()='Prompts:']/following-sibling::div//textarea)[1]").set "test question"
    find(:xpath, "//label[text()='Correct answers:']/following-sibling::table//input").set "answer"
    click_button "Save"
  end

  def card_name
    card_name = find(:css, '.card-front').text
    card_name
  end

  def select_dropdown_and_check_result_area(option_value, column, default_option_value)
    find(:xpath, "//option[text()='#{option_value}']").click
    expect(page).to_not have_css ".fa-spin"
    WaitUtil.wait_for_condition("language", :timeout_sec => 40, :delay_sec => 0.5) do
      expect(page).to_not have_content 'Loading...'
      sleep(0.5)
    end
    selected_language = page.all(:xpath, "//thead[//th[text()='#{option_value}']]/following-sibling::tbody//td[#{column}]").map { |language| language.text }
    selected_language.each do |each_language|
      expect(each_language).to eq find(:xpath, "//option[text()='#{option_value}']").text
    end
    find(:xpath, "//option[text()='#{default_option_value}']").click
    expect(page).to_not have_css ".fa-spin"
  end

  def select_dropdown_and_check_against_hash(option_value, column, default_option_value)
    if(option_value.eql?('Me'))
      find(:xpath, "//label[text()='Author']/following-sibling::select/option[text()='Me']").click
    else
      find(:xpath, "//option[text()='#{option_value}']").click
    end

    expect(page).to_not have_css ".fa-spin"

    WaitUtil.wait_for_condition('Loading results area', :timeout_sec => 40, :delay_sec => 0.5) do
      expect(page).to_not have_content 'Loading...'
      sleep(0.5)
    end

    selected_language = page.all(:xpath, "//thead[//th[text()='Language']]/following-sibling::tbody//td[#{column}]").map { |language| language.text }
    selected_language.each do |each_language|
      expect(each_language).to eq AebHelpers::COLUMN_VALUES[option_value]
    end

    if(option_value.eql?('Me'))
      find(:xpath, "//label[text()='Author']/following-sibling::select/option[text()='#{default_option_value}']").click
    else
      find(:xpath, "//option[text()='#{default_option_value}']").click
    end

    expect(page).to_not have_css ".fa-spin"
  end

  def select_next_option(filter_one, filter_two)
    select_option_initializer = [1, 1, 1, 2]
    $current_option_state ||= select_option_initializer
    first_option_values = page.all(:xpath, "//label[text()='#{filter_one}']/following-sibling::select/option").map { |first| first.text }
    second_option_values = page.all(:xpath, "//label[text()='#{filter_two}']/following-sibling::select/option").map { |second| second.text }
    filters = [filter_one, filter_two]
    option_values = [first_option_values, second_option_values]

    find(:xpath, "//option[text()='All Languages']").click
    expect(page).to_not have_css ".fa-spin"
    find(:xpath, "//option[text()='All Levels']").click
    expect(page).to_not have_css ".fa-spin"
    find(:xpath, "//option[text()='All States']").click
    expect(page).to_not have_css ".fa-spin"
    find(:xpath, "//label[text()='Author']/following-sibling::select/option[text()='Anyone']").click
    expect(page).to_not have_css ".fa-spin"

    filters.zip(option_values).each do |filter_value, option_value|
      case filter_value
        when 'Language'
          $current_option_state[0] += 1
          if ($current_option_state[0] == option_value.size) then
            $current_option_state[0] = select_option_initializer[0]
          end
          find(:xpath, "//option[text()='#{option_value[$current_option_state[0]]}']").click

        when 'Level'
          $current_option_state[1] += 1
          if ($current_option_state[1] == option_value.size) then
            $current_option_state[1] = select_option_initializer[1]
          end
          find(:xpath, "//option[text()='#{option_value[$current_option_state[1]]}']").click

        when 'State'
          $current_option_state[2] += 1
          if ($current_option_state[2] == option_value.size) then
            $current_option_state[2] = select_option_initializer[2]
          end
          find(:xpath, "//option[text()='#{option_value[$current_option_state[2]]}']").click

        when 'Author'
          $current_option_state[3] += 1
          if ($current_option_state[3] == option_value.size) then
            $current_option_state[3] = select_option_initializer[3]
          end
          find(:xpath, "//option[text()='#{option_value[$current_option_state[3]]}']").click
      end


      expect(page).to_not have_css ".fa-spin"
      WaitUtil.wait_for_condition('Loading results area', :timeout_sec => 40, :delay_sec => 0.5) do
        expect(page).to_not have_content 'Loading...'
        sleep(0.5)
      end

      if page.has_no_content?("No results matching your search")
        case filter_value
          when 'Language'
            selected_filter = page.all(:xpath, "//thead[//th[text()='#{filter_value}']]/following-sibling::tbody//td[#{COLUMN_POSITION[filter_value]}]").map { |filter| filter.text }
            selected_filter.each do |each_value|
              expect(each_value).to eq option_value[$current_option_state[0]]
            end
          when 'Level'
            selected_filter = page.all(:xpath, "//thead[//th[text()='#{filter_value}']]/following-sibling::tbody//td[#{COLUMN_POSITION[filter_value]}]").map { |filter| filter.text }
            selected_filter.each do |each_value|
              expect(each_value).to eq option_value[$current_option_state[1]]
            end
          when 'State'
            selected_filter = page.all(:xpath, "//thead[//th[text()='#{filter_value}']]/following-sibling::tbody//td[#{COLUMN_POSITION[filter_value]}]").map { |filter| filter.text }
            selected_filter.each do |each_value|
              expect(each_value).to eq COLUMN_VALUES[option_value[$current_option_state[2]]]
            end
          when 'Author'
            selected_filter = page.all(:xpath, "//thead[//th[text()='#{filter_value}']]/following-sibling::tbody//td[#{COLUMN_POSITION[filter_value]}]").map { |filter| filter.text }
            selected_filter.each do |each_value|
              expect(each_value).to eq COLUMN_VALUES[option_value[$current_option_state[3]]]
            end
        end
      end
    end
  end

  def user_is_at_advanced_english_editor
    expect(page).to have_xpath "//li[@class='active']/a[contains(text(),'Lessons')]"
    expect(page).to have_content 'Filters'
    expect(page).to have_xpath "//h4[contains(text(),'Displaying 20 /')]"
    expect(page).to have_content 'New Lesson'
  end

  def go_to_lesson_and_check_id(first_lesson_id)
    expect(page).to have_content 'New Lesson'
    find(:xpath,"//div[@class='input-group']/input").set first_lesson_id
    find(:xpath,"//button[contains(@class,'btn-default')]").click
    expect(page).to have_content first_lesson_id
    click_on 'Lessons'
    expect(page).to have_content 'New Lesson'
  end

  def go_to_lesson_and_check_name(lesson_index)
    lesson_name = find(:xpath, "//tr[contains(@class,'clickable')][#{lesson_index}]/td[1]").text
    find(:xpath, "//tr[contains(@class,'clickable')][#{lesson_index}]/td[1]").click
    expect(page).to have_content lesson_name
    click_on 'Lessons'
    expect(page).to have_content 'New Lesson'
  end

  def activity_progress_bar_indicates(status)
    $progress_count ||= 1
    expect(page).to have_css(".progress-indicator.#{status}", :count => $progress_count)
    if(['correct', 'incorrect'].include?(status))
      $progress_count += 1
    end
  end

  def reset_activity_counter
    $progress_count = 1
  end

end