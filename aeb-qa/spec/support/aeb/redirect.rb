require 'callisto/oauth/redirect'

module AEB
  module Redirect
    def self.advanced_english_login_link(username, password, environment, redirect_uri)
      Callisto::OAuth::Redirect.redirect_uri(username, password, environment, redirect_uri, 'b34edf462e7d9937f502e638b1b056ab')
    end
  end
end
