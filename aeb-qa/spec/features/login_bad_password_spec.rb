require 'spec_helper'

describe 'login', type: :feature do

  before do
    go_to_launchpad
  end

  it 'does not allow user with incorrect password', :qa2only => true do
    login(USER_WITH_INCORRECT_PASSWORD)
    expect(page).to have_content 'That email or password is incorrect.'
  end


  it 'allows a user with correct password', :qa2only => true do
    login(VALID_LEARNER)
    expect(page).to have_content 'Choose a product'
    signout
  end

end

