require 'spec_helper'

describe 'login', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows a learner to login', :qa2only => true do
    AebHelpers::CHECK_TABS.each do |name, value|
      expect(page).to have_content value
    end

    signout

    browser = Capybara.current_session.driver.browser
    browser.manage.delete_all_cookies

    login_as_user_and_go_to_aeb(VALID_LEARNER)

    AebHelpers::CHECK_TABS.each do |name, value|
      expect(page).to have_content value
    end

    signout
  end
end

