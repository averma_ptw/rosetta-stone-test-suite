require 'spec_helper'

describe 'extended writing', type: :feature do
  before do
    create_user_and_select_goals(AebHelpers::TEST_LESSON2[:goal_name])
  end

  it 'has a working skip button', :qa2only => true do
    wait_for_loaded
    select_goal_and_lesson(AebHelpers::TEST_LESSON2[:goal_name], AebHelpers::TEST_LESSON2[:lesson1_name])
    click_on('Extended writing')

    expect(page).to have_content "Enter your response here..." # verifying that the response field is blank
    expect(page).to have_xpath '//button[(@disabled=\'disabled\') and text()=\'Get Feedback\']' # verifying that the Get Feedback button is disabled

    find('.ng-pristine').set("hi") # enter some text into text field

    expect(page).to have_content('Skip') # check whether skip button is present
    expect(page).to_not have_xpath '//button[(@disabled=\'disabled\') and text()=\'Get Feedback\']' # verifying that the Get Feedback button is enabled

    next_activity_name = find(:xpath, "//a[@class='nav-item ng-scope disabled'][span[text()='Extended writing']]/following-sibling::a[1]/span").text # get next activity text
    click_button 'Skip'

    check_for_next_activity_button next_activity_name
    find(:xpath, "//span[text()='Extended writing']").click
    click_button 'Skip'
    check_for_next_activity_button next_activity_name

    click_on 'My goals'
    signout

  end
end


