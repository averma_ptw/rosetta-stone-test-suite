require 'spec_helper'

describe 'activity editor', type: :feature do

  it 'allows a user to login and create new activity set with objectives', :qa2only => true do

    login_as_user_and_go_to_editor(VALID_AUTHOR)

    expect(page).to have_content("New Lesson")
    click_button "New Lesson"

    number_of_objectives = check_new_lesson_objectives

    # Setting new lesson options
    find(:xpath, "//div[label[text()='Title']]//input[contains(@class,'form-control')]").set "Test Lesson"
    find(:xpath, "//option[text()='English (American)']").click
    find(:xpath, "//option[text()='B1']").click

    expect(page).to_not have_xpath "//button[@ng-click='save()' and @disabled='']" # Save is enabled
    click_button "Save"
    expect(page).to have_content "Validation errors"

    create_a_writing_practice_activity

    click_on "Info"

    find(:xpath, "//textarea[@placeholder='Objective 1']").set "test objective 1"
    click_button "Save"
    expect(page).to_not have_content "Validation errors"

    for textarea in 1..number_of_objectives
      find(:xpath, "//textarea[@placeholder='Objective #{textarea + 1} (optional)']").set "test objective #{textarea + 1}"
    end

    click_button "Save"
    expect(page).to_not have_content "Validation errors"
    signout
  end
end