require 'spec_helper'

describe 'learning activities', type: :feature do
  before do
    login_as_user_and_go_to_aeb('sxu+LEARNER@rosettastone.com', 'password')
  end

  it 'can be entered by selecting your course and activity', :prodonly => true, :feature => 'signin' do
    expect(page).to have_content 'A Persuasive Presentation'
    click_on 'A Persuasive Presentation, Part II'
    expect(page).to have_content 'After completing this lesson, you\'ll be able to'
    expect(page).to have_content 'use helpful vocabulary'
    # page.save_screenshot('activitysetintro.png')

    click_on 'Continue'

    expect(page).to have_content 'Next Activity'
  end
end
