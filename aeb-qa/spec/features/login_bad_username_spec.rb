require 'spec_helper'

describe 'login', type: :feature do

  before do
    go_to_launchpad
  end

  it 'does not allow a user with incorrect username', :qa2only => true do
    login(USER_WITH_INCORRECT_USERNAME)
    expect(page).to have_content 'That email or password is incorrect.'
  end


  it 'allows user with correct username and password', :qa2only => true do
    login(VALID_LEARNER)
    expect(page).to have_content 'Choose a product'
    signout
  end

end

