require 'spec_helper'

describe 'Live tutoring sessions', type: :feature do
  before do
    login_as_user_and_go_to_aeb('bevans+127rollout@rosettastone.com', 'asdfgh')
  end

  it 'click everything on the Live tutoring sessions tab', :prodonly => true do
    expect(page).to have_css '.goal-title-column'
    click_on 'Live tutoring sessions'
    within '.available-sessions-container' do
      click_on 'Schedule'
    end
    expect(page).to have_css '.modal.fade.live-sessions-scheduler-modal.in'
    click_on 'Done'
    within '.available-sessions-container' do
      click_on 'Schedule'
    end
    expect(page).to have_css '.modal.fade.live-sessions-scheduler-modal.in'
    find('.close-button').click
    click_on 'Check my system'
    #within('.modal-dialog.ng-scope') {click_button '.activity-button.secondary.ng-isolate-scope'}
    #click_on 'Check my system'
    expect(page).to have_css '.modal.fade.modal-medium.system-check-modal.in'
    find('.close-button').click
    click_on 'Check my system'
    click_on 'Close'
    click_on 'See tutorial'
    expect(page).to have_css '.modal.fade.modal-medium.video-modal.in'
    within('.modal.fade.modal-medium.video-modal.in') do
      find('.close-button').click
    end
  end
end
