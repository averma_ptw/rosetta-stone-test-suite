require 'spec_helper'

describe 'login', type: :feature do

  it 'allows an admin to login', :qa2only => true do

    login_as_user_and_go_to_aeb(VALID_ARIA_ADMIN)
    expect(page).to have_content('My goals')
    signout

    login(VALID_ARIA_ADMIN)
    @wait.until { launch_pad_elements(:aria_button) }
    click_on 'Administrator Tools'

    expect(page).to have_content('Organization Reports')

    signout_from_admin_tools
  end

end

