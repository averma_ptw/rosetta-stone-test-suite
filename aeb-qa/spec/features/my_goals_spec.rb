require 'spec_helper'

describe 'my goals', type: :feature do
  before do
    create_user_and_select_goals(AebHelpers::TEST_LESSON[:goal_name])
  end

  it 'allows a learner to take a test activity and cover pass and fail scenarios', :qa2only => true do

    wait_for_loaded
    lesson_names = page.all(:xpath, "//div[@class='goal-row'][div[@class='expand-indicator-column expanded']]/following-sibling::ul//div[@class='lesson-title-column ng-binding']").map { |lesson| lesson.text }

    #clicking on uncollpased goal
    find(".expand-indicator-column.expanded").click
    lesson_names.each do |lessons|
      expect(page).to_not have_content lessons
    end

    #clicking on collapsed goal
    find(:xpath, "(//div[@class='expand-indicator-column'])[1]").click
    lesson_names.each do |lessons|
      expect(page).to have_content lessons
    end

    #clicking on a lesson
    @driver.find_element(:xpath, "//div[text()='#{lesson_names[0]}']").click
    expect(page).to have_xpath "//a[@ng-bind='activitySet.title' and text()='#{lesson_names[0]}']"

    click_on "My goals"
    expect(page).to have_css ".my-goals-view"

    #clicking on an activity which has no progress
    select_goal_and_lesson(AebHelpers::TEST_LESSON[:goal_name], AebHelpers::TEST_LESSON[:lesson4_name])
    expect(page).to have_content AebHelpers::TEST_LESSON[:lesson4_name]
    click_on 'Usage practice'

    #make some progress in an activity
    answering_activity_either_fully_correct_or_fully_wrong(AebHelpers::TEST_QUESTION_DETAILS_FOR_MY_GOALS, AebHelpers::TEST_ANSWER_DETAILS_FOR_MY_GOALS, number_of_questions: 1)

    click_on "My goals"
    expect(page).to have_css ".my-goals-view"

    #verifying there is some progress on the activity
    expect(page).to_not have_xpath("//div[text()='#{AebHelpers::TEST_LESSON[:lesson4_name]}']/following-sibling::div//li[@style='width: 0%;']")

    #verifying the color of the progress bar(yellow)
    verify_color_code("//div[text()='#{AebHelpers::TEST_LESSON[:lesson4_name]}']//following-sibling::div//li", "background-color", "#ffe25f")

    #take test and score below 80%
    select_goal_and_lesson(AebHelpers::TEST_LESSON[:goal_name], AebHelpers::TEST_LESSON[:lesson4_name])
    click_on 'Test'
    click_button 'Take Test'
    while page.has_content?('Submit')
      click_button "Submit"
    end
    click_on "My goals"

    #retake test appears next to progress bar in red colour
    expect(page).to have_xpath("//div[@class='lesson-progress-column']/following-sibling::div//span[text()='Retake test']")
    verify_color_code("//div[text()='#{AebHelpers::TEST_LESSON[:lesson4_name]}']//following-sibling::div//span[text()='Retake test']", "color", "#db4628")

    #take the test again and score above 80%
    select_goal_and_lesson(AebHelpers::TEST_LESSON[:goal_name], AebHelpers::TEST_LESSON[:lesson4_name])
    click_on 'Test'
    click_button 'Take Test'
    answer_test_activity(AebHelpers::TEST_QUESTIONS_FOR_MY_GOALS, AebHelpers::TEST_ANSWERS_FOR_MY_GOALS)
    click_on "My goals"

    #verifying the color code of the progress bar and checking for the green mark next to progress bar
    verify_color_code("//div[text()='#{AebHelpers::TEST_LESSON[:lesson4_name]}']//following-sibling::div//li", "background-color", "#8dc63f")
    expect(page).to have_xpath("//div[text()='#{AebHelpers::TEST_LESSON[:lesson4_name]}']//following-sibling::div[@class='lesson-status-column passed']")

    click_on 'My goals'
    signout
  end
end
