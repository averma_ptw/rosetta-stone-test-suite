require 'spec_helper'

describe 'support', type: :feature do

  it 'allows the user to access support page', :qa2only => true do

    user_details = create_user_and_register_details
    select_level_for_new_user("B1")
    select_first_three_goals_for_new_user

    user_email = user_details[:email].gsub(/[(@)]/, '%40') # preparing email id for GET request

    current_window = page.driver.browser.window_handles.first

    find(:xpath, "//a[text()='Support']").click
    new_window = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(new_window)

    # check current url
    expect(page.driver.current_url).to eq "#{AebHelpers::SUPPORT_PAGE_URL}?email=#{user_email}&name=#{user_details[:firstName]}+#{user_details[:lastName]}&product=Advanced+English+for+Business"

    page.driver.browser.switch_to.window(current_window)
    signout
  end
end