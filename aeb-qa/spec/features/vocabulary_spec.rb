require 'spec_helper'

describe 'vocabulary', type: :feature do
  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'navigates user to vocabulary activity and runs the activity', :qa2only => true do
    select_goal_and_lesson(AebHelpers::TEST_LESSON3[:goal_name], AebHelpers::TEST_LESSON3[:lesson1_name])
    find(:xpath, "//span[text()='Vocabulary']").click

    expect(page).to have_content('Flip Card')
    click_button 'Flip Card'

    expect(page).to_not have_content 'Flip Card'
    expect(page).to have_content('Not Yet')
    expect(page).to have_content('I Understand')

    click_button 'I Understand'
    activity_progress_bar_indicates('correct')

    take_note_of_the_card1 = card_name
    click_button 'Flip Card'
    click_button 'Not Yet'
    activity_progress_bar_indicates('inProgress')

    loop do # until the noted card appears
      break if page.has_content? take_note_of_the_card1
      click_button 'Flip Card'
      click_button 'Not Yet'
    end

    expect(page).to have_content take_note_of_the_card1

    click_button 'Flip Card'
    click_button 'I Understand'
    activity_progress_bar_indicates('correct')

    while page.has_content?('Flip Card') # until you come to the same card
      take_current_card = card_name
      expect(take_current_card).to_not have_content take_note_of_the_card1
      click_button 'Flip Card'
      click_button 'Not Yet'
      expect(page).to_not have_content 'Not Yet'
    end

    click_button 'Try Again'
    click_button 'Flip Card'
    click_button 'I Understand'

    expect(page).to have_content 'Flip Card'
    find("button[title='Restart the activity']").click
    reset_activity_counter

    activity_progress_bar_indicates('inProgress')

    begin
      click_button 'Flip Card'
      click_button 'I Understand'
    end while  page.has_no_css?('.progress-indicator.correct', :count => 2) # fill up progress bar

    take_note_of_the_card2 = card_name

    while page.has_no_content? take_note_of_the_card2 # until noted card appears
      click_button 'Flip Card'
      click_button 'Not Yet'
    end
    expect(page).to have_content take_note_of_the_card2

    3.times do
      click_button 'Flip Card'
      click_button 'Not Yet'
    end

    check_help_button_functionality

    find(:css, ".flag-content-button").click
    expect(page).to have_css '.modal-dialog' # flag popup
    number_of_radio_buttons = page.all("input[type='radio']").size

    for i in 1.. number_of_radio_buttons
      find(:xpath, "(//input[@type='radio'])[#{i}]").click
      expect(page).to have_css('.ng-dirty', :count => i) # Only one option is enabled
    end

    if find(:xpath, "//span[text()='Other']").click
      expect(page).to have_css 'textarea' # text field appears
      expect(page).to have_xpath "//button[text()='Report Problem' and @disabled='disabled']" # Report Problem button is deactivated
    end

    find(:xpath, "//span[text()='Other']").click # Click on the Other Field
    find(:css, 'textarea').set("type text")
    expect(page).to_not have_xpath "//button[text()='Report Problem' and @disabled='disabled']" # Report Problem button is activated


    click_button 'Cancel'
    expect(page).to_not have_css ".modal-dialog" # the pop up is disabled

    find(:css, ".flag-content-button").click # click on flag button
    find(:css, ".close-button").click
    expect(page).to_not have_css ".modal-dialog" #the pop up is disabled


    find(:css, ".flag-content-button").click # click on flag button
    find(:xpath, "//span[text()='Other']").click
    find(:css, 'textarea').set("Re enter text")
    click_button 'Report Problem'

    find(:css, ".flag-content-button").click # Report a different problem
    find(:xpath, "//span[text()='Other']").click
    find(:css, 'textarea').set("submit different enter text")
    click_button 'Report Problem'

    find("button[title='Restart the activity']").click
    reset_activity_counter

    begin
      click_button 'Flip Card'
      click_button 'Not Yet'
    end while page.has_no_css?('.progress-indicator.incorrect', :count => 3) # get most of them wrong

    click_button 'Flip Card'
    click_button 'I Understand'

    click_button 'Try Again'
    reset_activity_counter
    activity_progress_bar_indicates('inProgress')

    begin
      click_button 'Flip Card'
      click_button 'I Understand'
    end while page.has_no_css?('.progress-indicator.correct', :count => 3) # get most of them right

    click_button 'Flip Card'
    click_button 'Not Yet'

    expect(page).to have_css '.graph.previous' # old score graph
    expect(page).to have_xpath "//div[text()='Your last attempt']"
    expect(page).to have_css ".attempt.previous" # time stamp appears next to your old score
    expect(page).to have_css '.graph.current' # current score graph
    expect(page).to have_css '.correct-count.ng-binding' # score is displayed
    expect(page).to have_content "You're improving"

    test_next_activity=find(:xpath, "//a[@class='nav-item ng-scope disabled'][span[text()='Vocabulary']]//following-sibling::a[1]//span").text
    find(:xpath, "//button[text()='Next Activity']").click

    check_for_next_activity_button(test_next_activity)

    find(:xpath, "//span[text()='Vocabulary']").click
    click_button 'Flip Card'
    find(:xpath, "//a[@class='nav-item ng-scope disabled'][span[text()='Vocabulary']]//following-sibling::a[1]//span").click # click on next activity

    check_for_next_activity_button(test_next_activity) #You are taken out of Vocabulary without error
    expect(page).to_not have_xpath("//span[text()='Vocabulary']/following-sibling::div/div[@style='width: 0%;']") # progress bar is yellow

    click_on "My goals"
    signout
  end
end


