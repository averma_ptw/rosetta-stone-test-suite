require 'spec_helper'

describe 'expert review', type: :feature do
  before do
    login_as_user_and_go_to_aeb(VALID_COACH)
    click_on("Expert review")
  end

  it 'allows a coach to sort and access feedback requests', :qa2only => true do
    # By default, the Filters should be set to All Levels and All practice types - unable to check this as there is no selected tag(xml) in options
    choose_cefr_level "All levels"
    choose_practice_types "All practice types"

    # checks the level or type or both which is selected
    check_values_in_cefr_level_column("B1", "B2", "C1")
    check_values_in_practice_type_column("Extended speaking", "Extended writing")

    click_on_any_review
    expect(page).to have_css ".feedback-request-view"

    click_on 'Expert review'
    wait_for_loaded

    #Selecting category 'B1' and 'Extending speaking'
    choose_cefr_level "B1"
    check_values_in_cefr_level_column "B1"
    choose_practice_types "Extended speaking"

    #Checking category 'B1' and 'Extended speaking'
    check_values_in_cefr_level_column "B1"
    check_values_in_practice_type_column "Extended speaking"

    #Selecting 'Extended writing'
    choose_practice_types "Extended writing"

    #Checking category 'B1' and 'Extended writing'
    check_values_in_cefr_level_column "B1"
    check_values_in_practice_type_column "Extended writing"

    #Selecting category 'B2' and 'All practice types'
    choose_cefr_level "B2"
    choose_practice_types "All practice types"

    #Checking category 'B2' and both 'Extended writing' and 'Extended speaking'
    check_values_in_cefr_level_column "B2"
    check_values_in_practice_type_column("Extended writing", "Extended speaking")

    #Selecting category 'Extended speaking'
    choose_practice_types "Extended speaking"

    #Checking category 'B2' and 'Extended speaking'
    check_values_in_cefr_level_column "B2"
    check_values_in_practice_type_column "Extended speaking"

    #Selecting category 'Extended writing'
    choose_practice_types "Extended writing"

    #Checking category 'B2' and 'Extended writing'
    check_values_in_cefr_level_column "B2"
    check_values_in_practice_type_column "Extended writing"

    #Selecting category 'C1' and 'All practice types'
    choose_cefr_level "C1"
    choose_practice_types "All practice types"

    #Checking category 'C1' and both 'Extended writing' and 'Extended speaking'
    check_values_in_cefr_level_column "C1"
    check_values_in_practice_type_column("Extended writing", "Extended speaking")

    #Selecting category 'Extended speaking'
    choose_practice_types "Extended speaking"

    #Checking category 'C1' and 'Extended speaking'
    check_values_in_cefr_level_column "C1"
    check_values_in_practice_type_column "Extended speaking"

    #Selecting category 'Extended writing'
    choose_practice_types "Extended writing"

    #Checking category 'C1' and 'Extended writing'
    check_values_in_cefr_level_column "C1"
    check_values_in_practice_type_column "Extended writing"

    #Reset category to 'All levels' and 'All practice types'
    choose_cefr_level "All levels"
    choose_practice_types "All practice types"

    click_on_any_review
    page.evaluate_script('window.history.back()') #click on back button
    review_five_feedback_requests

    #Repeating the above action for 'B1' and 'Extended writing'
    choose_cefr_level "B1"
    choose_practice_types "Extended writing"

    click_on_any_review
    page.evaluate_script('window.history.back()')
    review_five_feedback_requests

    #Repeating the above action for 'B2' and 'Extended speaking'
    choose_cefr_level "B2"
    choose_practice_types "Extended speaking"

    click_on_any_review
    page.evaluate_script('window.history.back()')
    review_five_feedback_requests

    #Repeating the above action for 'C1' and 'Extended writing'
    choose_cefr_level "C1"
    choose_practice_types "Extended writing"

    click_on_any_review
    page.evaluate_script('window.history.back()')
    review_five_feedback_requests

    signout
  end
end