require 'spec_helper'

describe 'signing in', type: :feature do

  # FIXME: add ensure block that logs out of aria/launchpad or clears cookies. capybara just clears cookies of last site it ends up on, and nothing if something fails.
  it "works marvelously", :feature => 'signin' do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
    expect(page).to have_content 'My goals'
    signout
  end
end

