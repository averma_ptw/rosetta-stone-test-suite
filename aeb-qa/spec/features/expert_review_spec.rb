require 'spec_helper'

describe 'expert review', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_COACH)
    click_on 'Expert review'
  end

  it "allows a coach to see a list of learner submissions", :feature => 'expertreview' do
    expect(page).to have_css(feedback_request_submission_selector)
    signout
  end

  it "allows a coach to view a particular learner submission", :feature => 'expertreview' do
    expect(page).to have_css(feedback_request_submission_selector)
    first(feedback_request_submission_selector).click
    expect(page).to have_content("I'm not able to rate this.")
    signout
  end

end

