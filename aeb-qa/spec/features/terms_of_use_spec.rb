require 'spec_helper'

describe 'terms of use', type: :feature do
  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows a user to access terms of use page', :qa2only => true do

    current_window = page.driver.browser.window_handles.first

    find(:xpath, "//a[text()='Terms of Use']").click
    new_window = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(new_window)

    expect(page.driver.current_url).to eq AebHelpers::TERMS_OF_USE_URL

    page.driver.browser.switch_to.window(current_window)
    signout
  end
end