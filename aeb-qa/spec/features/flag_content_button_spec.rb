require 'spec_helper'

describe 'clicking flag content button', type: :feature do
  before do
    login_as_user_and_go_to_aeb('bevans+127rollout@rosettastone.com', 'asdfgh')
  end

  it 'causes flag content popup to appear', :prodonly => true do
    click_on 'Going "Off Script," Part II'
    click_on 'Usage'
    expect(page).to have_css '.grammer-drill-card'
    within('.control-bar') {click_button 'Report a problem with this page'}
    expect(page).to have_css '.flag-content-reason'
    find(:xpath, "//input[following-sibling::span[1][. = 'Poor audio quality']]").click
    #locate('input + span:contains(Poor audio quality)').click
  end
end
