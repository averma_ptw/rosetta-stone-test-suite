require 'spec_helper'

describe 'first time experience', type: :feature do

  context 'when user takes placement test' do
    it "system shows a message thanking the user for taking the placement test", :qa1only => true do
      user = create_user(:how_level_was_chosen => 'placementTest')
      email = user[:email]
      password = SeleniumHelper::Settings.environment_settings["valid_password"]
      # puts "creds: #{email} / #{password}"
      login_as_user_and_go_to_aeb(email, password)
      expect(page).to have_content('Thanks for completing the placement test!')
      signout
    end
  end

  context 'when user self-selects CEFR level' do
    it "system does not show a message thanking the user for taking the placement test", :qa1only => true do
      user = create_user(:how_level_was_chosen => 'self')
      email = user[:email]
      password = SeleniumHelper::Settings.environment_settings["valid_password"]
      # puts "creds: #{email} / #{password}"
      login_as_user_and_go_to_aeb(email, password)
      expect(page).to_not have_content('Thanks for completing the placement test!')
      signout
    end
  end

end

