require 'spec_helper'

describe 'change password', type: :feature do
  before do
    login_as_user_and_go_to_aeb('bevans+passwordtest@rosettastone.com', 'asdfgh')
  end

  it 'works', :prodonly => true do
    Capybara.default_wait_time = 90 # FIXME. remove this
    expect(page).to have_css '.goal-title-column'
    click_on 'My profile'
    expect(page).to have_content 'Brandon Evans'
    within('.section.password-section') {click_on 'Edit'}
    within('.section.password-section') {fill_in 'oldPassword', :with =>'asdfgh'}
    within('.section.password-section') {fill_in 'newPassword', :with => 'asdfghj'}
    within('.section.password-section') {fill_in 'confirmNewPassword', :with => 'asdfghj'}
    click_on 'Save'
    within('.section.password-section') { expect(page).to have_content('Edit') }

    signout

    login_as_user_and_go_to_aeb('bevans+passwordtest@rosettastone.com', 'asdfghj')
    expect(page).to have_css '.goal-title-column'
    click_on 'My profile'
    expect(page).to have_content 'Brandon Evans'
    within('.section.password-section') {click_on 'Edit'}
    within('.section.password-section') {fill_in 'oldPassword', :with =>'asdfghj'}
    within('.section.password-section') {fill_in 'newPassword', :with => 'asdfgh'}
    within('.section.password-section') {fill_in 'confirmNewPassword', :with => 'asdfgh'}
    click_on 'Save'
    within('.section.password-section') { expect(page).to have_content('Edit') }

    signout
  end
end
