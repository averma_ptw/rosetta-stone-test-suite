require 'spec_helper'

describe 'writing practice', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows a user to retry writing practice activity', :qa2only => true do

    select_goal_and_lesson(AebHelpers::TEST_LESSON2[:goal_name], AebHelpers::TEST_LESSON2[:lesson1_name])
    find(:xpath, "//span[text()='Writing practice']").click

    expect(page).to have_css '.ng-pristine' #Check for prompt
    click_button 'Check My Answer'
    expect(page).to have_css '.feedback-message'

    verify_buttons_try_again_and("Skip")
    click_button 'Try Again'
    expect(page).to have_css '.upper-canvas' #Beginning of activity screen

    expect(page).to have_content 'Check My Answer'
    expect(page).to have_css '.ng-pristine'
    expect(page).to_not have_css '.feedback-message'

    click_button('Check My Answer')
    expect(page).to have_css '.feedback-message'
    verify_buttons_try_again_and("Skip")

    click_button 'Skip'
    expect(page).to have_css '.upper-canvas' #Verify end of activity screen

    verify_buttons_try_again_and("Next Activity")

    click_button('Try Again')
    expect(page).to have_css '.upper-canvas' #You're taken to the beginning of activity screen
    expect(page).to have_content('Check My Answer')
    expect(page).to have_css '.ng-pristine'

    click_on 'My goals'
    signout
  end
end

