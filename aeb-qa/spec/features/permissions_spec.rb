require 'spec_helper'

describe 'permissions system', type: :feature do
  it "allows an author to access the editor", :feature => 'permissions' do
    login_as_user_and_go_to_aeb(VALID_AUTHOR)
    expect(page).to have_content 'My goals'
    expect(page).to have_content 'Course editor'
    signout
  end

  it "does not allow a learner to access the editor", :feature => 'permissions' do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
    expect(page).to have_content 'My goals'
    expect(page).to_not have_content 'Course editor'
    signout
  end
end

