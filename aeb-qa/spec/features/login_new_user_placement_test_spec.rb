require 'spec_helper'

describe 'login', type: :feature do

  it 'allows a new user to login and get placed below b1 through placement test', :qa2only => true do

    create_user_and_take_placement_test("BELOW_B1")

  end

  it 'allows a new user to login and get placed at b1 through placement test', :qa2only => true do

    create_user_and_take_placement_test("B1")

  end

  it 'allows a new user to login and get placed at b2 through placement test', :qa2only => true do

    create_user_and_take_placement_test("B2")

  end

  it 'allows a new user to login and get placed at c1 through placement test', :qa2only => true do

    create_user_and_take_placement_test("C1")

  end
end

