require 'spec_helper'
require 'waitutil'

describe 'editor', type: :feature do

  it 'allows a user to sign into english editor and check filter options', :qa2only => true do

    login_as_user_and_go_to_editor(VALID_AUTHOR)
    expect(page).to_not have_css ".fa-spin"

    user_is_at_advanced_english_editor

    #These lines of code select the default values of the filters
    find(:xpath, "//option[text()='All Languages']").click
    raise 'Incorrect default value' if page.has_css?(".fa-spin")
    find(:xpath, "//option[text()='All Levels']").click
    raise 'Incorrect default value' if page.has_css?(".fa-spin")
    find(:xpath, "//option[text()='All States']").click
    raise 'Incorrect default value' if page.has_css?(".fa-spin")
    find(:xpath, "//label[text()='Author']/following-sibling::select/option[text()='Anyone']").click
    raise 'Incorrect default value' if page.has_css?(".fa-spin")
    find(:xpath, "//label[text()='Curator']/following-sibling::select/option[text()='Anyone']").click
    raise 'Incorrect default value' if page.has_css?(".fa-spin")

    # select various values from filters and check the result area
    select_dropdown_and_check_result_area('English (American)', 3, 'All Languages')
    select_dropdown_and_check_result_area('English (British)', 3, 'All Languages')
    select_dropdown_and_check_result_area('B1', 4, 'All Levels')
    select_dropdown_and_check_result_area('B2', 4, 'All Levels')
    select_dropdown_and_check_result_area('C1', 4, 'All Levels')
    select_dropdown_and_check_against_hash('Authoring', 5, 'All States')
    select_dropdown_and_check_against_hash('Pending Review', 5, 'All States')
    select_dropdown_and_check_against_hash('In Review', 5, 'All States')
    select_dropdown_and_check_against_hash('Approved', 5, 'All States')
    select_dropdown_and_check_against_hash('Published', 5, 'All States')
    select_dropdown_and_check_against_hash('Cleanup', 5, 'All States')
    select_dropdown_and_check_against_hash('Me', 2, 'Anyone')

    # first lesson present on the Editor homepage
    find(:xpath,"//tr[contains(@class,'clickable')][1]/td[1]").click
    expect(page).to have_content 'Info'
    first_lesson_id = find(:xpath,"//label[text()='ID']/following-sibling::div/div").text
    click_on 'Lessons'

    go_to_lesson_and_check_id(first_lesson_id)

    # select a combination of filters and observe the Results area
    select_next_option('Language', 'Level')
    select_next_option('Language', 'State')
    select_next_option('Language', 'Author')
    select_next_option('Level', 'State')
    select_next_option('Level', 'Author')
    select_next_option('State', 'Author')

    find(:xpath, "//option[text()='All Languages']").click
    expect(page).to_not have_css ".fa-spin"
    find(:xpath, "//option[text()='All Levels']").click
    expect(page).to_not have_css ".fa-spin"
    find(:xpath, "//option[text()='All States']").click
    expect(page).to_not have_css ".fa-spin"
    find(:xpath, "//label[text()='Author']/following-sibling::select/option[text()='Anyone']").click
    expect(page).to_not have_css ".fa-spin"

    # using waitutil as result area takes a long time to load
    WaitUtil.wait_for_condition('Loading results area', :timeout_sec => 40, :delay_sec => 0.5) do
      expect(page).to_not have_content 'Loading...'
      sleep(0.5)
    end

    # check first five lessons from homepage
    for lesson_index in 1..5
      go_to_lesson_and_check_name(lesson_index)
    end

    click_on 'New Lesson'
    expect(page).to have_content 'Learning objectives'
    click_on 'Lessons'
    click_button 'Discard changes'
    signout
  end
end