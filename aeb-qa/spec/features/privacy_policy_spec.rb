require 'spec_helper'

describe 'privacy policy', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows a user to access privacy policy page', :qa2only => true do

    current_window = page.driver.browser.window_handles.first

    click_on 'Online Interactive Product Privacy Policy'

    new_window = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(new_window)

    expect(page.driver.current_url).to eq AebHelpers::PRIVACY_POLICY_URL

    page.driver.browser.switch_to.window(current_window)
    signout
  end
end

