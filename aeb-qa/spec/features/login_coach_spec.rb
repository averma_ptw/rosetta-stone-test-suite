require 'spec_helper'

describe 'login', type: :feature do

  before do
    go_to_launchpad
  end

  it 'allows a coach to login', :qa2only => true do
    login_as_user_and_go_to_aeb(VALID_COACH)
    wait_for_loaded
    signout
  end

end

