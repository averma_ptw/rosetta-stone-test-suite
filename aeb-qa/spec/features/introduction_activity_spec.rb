require 'spec_helper'

describe 'introduction', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows the user to access introduction activity', :qa2only => true do

    select_goal_and_lesson(AebHelpers::TEST_LESSON[:goal_name], AebHelpers::TEST_LESSON[:lesson1_name])

    expect(page).to have_content 'After completing this lesson'
    click_on 'Introduction'

    expect(page).to_not have_content 'After completing this lesson'
    click_button 'Continue'

    expect(page).to have_content 'Next Activity'
    click_button 'Next Activity'

    # Check next activity name
    test_next_activity = find(:xpath, "//a[@class='nav-item ng-scope disabled'][span[text()='Introduction']]/following-sibling::a[1]/span").text
    expect(page).to have_xpath "//a[@class='nav-item ng-scope disabled'][span[text()='#{test_next_activity}']]"

    check_help_button_functionality

    number_of_radio_buttons = check_flag_content_and_click_on 'Cancel'
    check_flag_content_and_click_on 'close-button'

    for i in 1..number_of_radio_buttons # for all radio buttons
      report_problem_for_radio_button i
    end

    click_on 'My goals'
    signout
  end
end