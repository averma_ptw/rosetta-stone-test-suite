require 'spec_helper'

describe 'usage questions', type: :feature do

  before do
    create_user_and_select_goals AebHelpers::TEST_LESSON[:goal_name]
  end

  it 'allows a user to select correct and partially correct answers along with help and flag options', :qa2only => true do

    select_goal_and_lesson(AebHelpers::TEST_LESSON[:goal_name], AebHelpers::TEST_LESSON[:lesson3_name])
    find(:xpath, "//span[text()='Usage practice']").click

    answering_activity_either_fully_correct_or_fully_wrong(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::WRONG_ANSWER_DETAILS)
    expect(page).to have_css(".correct-count")

    verify_buttons_try_again_and 'Next Activity'
    click_button 'Try Again'

    answering_activity_either_fully_correct_or_fully_wrong(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::TEST_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Try Again'

    #Usage questions with partially correct answers twice
    finish_activity_with_half_answers_correct(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::TEST_ANSWER_DETAILS, AebHelpers::WRONG_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Try Again'

    finish_activity_with_half_answers_correct(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::TEST_ANSWER_DETAILS, AebHelpers::WRONG_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Try Again'

    answering_activity_either_fully_correct_or_fully_wrong(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::WRONG_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Next Activity'

    answering_activity_either_fully_correct_or_fully_wrong(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::TEST_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Next Activity'

    # Usage questions with partially correct answers twice
    finish_activity_with_half_answers_correct(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::TEST_ANSWER_DETAILS, AebHelpers::WRONG_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Next Activity'

    finish_activity_with_half_answers_correct(AebHelpers::TEST_QUESTION_DETAILS, AebHelpers::TEST_ANSWER_DETAILS, AebHelpers::WRONG_ANSWER_DETAILS)
    check_end_of_activity_and_click_on 'Next Activity'

    # Restart activity before completion, twice
    restart_activity_after_answering 1
    restart_activity_after_answering 2

    check_help_button_functionality

    number_of_radio_buttons = check_flag_content_and_click_on 'Cancel'

    check_flag_content_and_click_on 'close-button'

    for i in 1..number_of_radio_buttons # for all radio buttons
      report_problem_for_radio_button i
    end

    click_on 'My goals'
    signout
  end
end

