require 'spec_helper'

describe 'Usage', type: :feature do
  before do
    login_as_user_and_go_to_aeb('bevans+127rollout@rosettastone.com', 'asdfgh')
  end

  # i'm not sure it makes sense to tag tests that aren't working due to a product defect in this way. we'll iterate.
  it 'clicking I Understand and Not Yet buttons takes learner to next usage card', :prodonly => true, :brokenduetodefect => true, :defect => 'AEB-4409' do
    select_goal_and_lesson('Develop Customer Service Skills (C1)', 'Handling a Customer, Part II')
    expect(page).to have_css '.question-text'
    click_on 'Usage'
    expect(page).to have_css '.grammer-drill-card'
    find('.btn-help').click
    find('.btn-help').click
    within('.control-bar') {click_button 'Report a problem with this page'}
    expect(page).to have_css '.flag-options-wrapper'
    click_on 'Cancel'
    within('.control-bar') {click_button 'Report a problem with this page'}
    expect(page).to have_css '.flag-options-wrapper'
    find('.close-button').click
    expect(page).to_not have_css '.progress-indicator.correct'

    click_on 'I Understand'
    expect(page).to have_css '.progress-indicator.correct'
    within('.progress-view') {click_button 'Restart the activity'}

    # first question
    expect(page).to have_css '.grammer-drill-card'
    expect(page).to have_content('Not Yet')
    click_on 'Not Yet'

    16.times do |ii|
      #p "on question #{ii + 2}"
      expect(page).to have_css '.progress-indicator.inProgress'
      expect(page).to have_css '.grammer-drill-card'
      expect(page).to have_content('Not Yet')
      click_on 'Not Yet'
    end

    expect(page).to have_css '.correct-count'
    #expect(page).to have_css '.attempt-title' # FIXME. what's the intent here, bevans
    click_on 'Try Again'
    expect(page).to have_css '.grammer-drill-card'
    click_on 'I Understand'
    expect(page).to have_css '.progress-indicator.correct'
    expect(page).to have_css '.grammer-drill-card'
    click_on 'I Understand'
    expect(page).to have_css '.progress-indicator.correct'
    expect(page).to have_css '.grammer-drill-card'
    click_on 'I Understand'
    expect(page).to have_css '.progress-indicator.correct'
    expect(page).to have_css '.grammer-drill-card'
    click_on 'I Understand'
    expect(page).to have_css '.progress-indicator.correct'
    expect(page).to have_css '.grammer-drill-card'
    click_on 'I Understand'
    expect(page).to have_css '.progress-indicator.correct'
    expect(page).to have_css '.grammer-drill-card'
    click_on 'I Understand'
    expect(page).to have_css '.correct-count'
    click_on 'I Understand'
    expect(page).to have_css '.correct-count'
    #expect(page).to have_css '.attempt-title'
    click_on 'Next Activity'
    expect(page).to have_content 'Check My Answer'
  end
end
