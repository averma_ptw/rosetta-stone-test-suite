require 'spec_helper'

describe 'writing practice', type: :feature do
  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows a user to access skip button and end of activity screen', :qa2only => true do

    select_goal_and_lesson(AebHelpers::TEST_LESSON2[:goal_name], AebHelpers::TEST_LESSON2[:lesson1_name])
    find(:xpath, "//span[text()='Writing practice']").click

    expect(page).to have_css '.ng-pristine' # Writing practice challenge prompt
    click_button 'Check My Answer'

    verify_buttons_try_again_and("Skip")
    click_button 'Skip'
    expect(page).to have_css '.main-canvas.activity-completion-view' # End of activity screen

    verify_buttons_try_again_and("Next Activity")

    click_on 'My goals'
    signout

  end
end