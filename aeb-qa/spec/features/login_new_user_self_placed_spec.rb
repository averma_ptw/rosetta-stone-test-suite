require 'spec_helper'


describe 'login', type: :feature do

  it 'allows a new user to login and self place below b1', :qa2only => true do

    create_user_and_self_place_at 'BELOW_B1'

  end

  it 'allows a new user to login and self place at b1', :qa2only => true do

    create_user_and_self_place_at 'B1'

  end

  it 'allows a new user to login and self place at b2', :qa2only => true do

    create_user_and_self_place_at 'B2'

  end

  it 'allows a new user to login and self place at c1', :qa2only => true do

    create_user_and_self_place_at 'C1'

  end
end

