require 'spec_helper'

describe 'writing practice', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
  end

  it 'allows a user to access writing practice and next activity', :qa2only => true do

    select_goal_and_lesson(AebHelpers::TEST_LESSON2[:goal_name], AebHelpers::TEST_LESSON2[:lesson1_name])
    find(:xpath, "//span[text()='Writing practice']").click

    expect(page).to have_css '.ng-pristine' # Writing practice challenge prompt
    click_button 'Check My Answer'

    expect(page).to have_css '.feedback-message.ng-scope.ng-binding.wrong-answer' # Wrong answer
    click_button 'Skip'

    verify_buttons_try_again_and("Next Activity")
    click_button 'Next Activity'

    # Check next activity name
    test_next_activity = find(:xpath, "//a[@class='nav-item ng-scope disabled'][span[text()='Writing practice']]/following-sibling::a[1]/span").text
    expect(page).to have_xpath "//a[@class='nav-item ng-scope disabled'][span[text()='#{test_next_activity}']]"

    click_on 'My goals'
    signout
  end
end