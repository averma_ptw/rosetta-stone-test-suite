require 'spec_helper'

describe 'clicking every tab', type: :feature do
  before do
    login_as_user_and_go_to_aeb('bevans+127rollout@rosettastone.com', 'asdfgh')
  end

  it 'takes learner to appropriate screens', :prodonly => true do
    expect(page).to have_content 'Develop Customer Service Skills'
    click_on 'Add new goals'
    expect(page).to have_content 'Build English Skills for Emergency Services'
    click_on 'Live tutoring sessions'
    expect(page).to have_content 'Available Sessions'
    click_on 'Notifications'
    expect(page).to have_content 'Notifications'
    click_on 'My profile'
    expect(page).to have_content 'Brandon Evans'
    signout
    expect(page).to have_content 'Forgot password?'
  end
end
