require 'spec_helper'

describe 'my profile', type: :feature do

  before do
    login_as_user_and_go_to_aeb(VALID_LEARNER)
    click_on 'My profile'
  end

  it 'allows a learner to change password and login back', :qa2only => true do

    click_edit_on_password_section
    verify_password_section_expanded
    expect(page).to have_xpath "//div[@class='password-edit edit-mode module']//button[text()='Cancel']"
    verify_inactive_save_button

    click_inside_password_section("Cancel")
    verify_password_section_collapsed

    click_edit_on_password_section
    verify_password_section_expanded

    click_edit_on_cefr_level_section
    verify_password_section_collapsed
    # The section user clicked Edit on is open
    verify_cefr_level_section_expanded

    click_edit_on_password_section
    verify_password_section_expanded

    # Enter invalid information in the Old password, New password and Re-enter new password fields
    fill_in_passwords(INVALID_PASSWORD, INVALID_PASSWORD)
    click_edit_on_cefr_level_section
    verify_password_section_collapsed
    verify_cefr_level_section_expanded

    click_edit_on_password_section
    # Enter valid information in the Old password, New password and Re-enter new password fields
    current_time_year = Time.new.year.to_s
    fill_in_passwords(VALID_PASSWORD, VALID_PASSWORD + current_time_year)

    click_inside_password_section("Save")
    wait_for_loaded
    verify_password_section_collapsed
    click_edit_on_password_section
    signout

    # The password change was successful
    login_as_user_and_go_to_aeb(VALID_LEARNER[:username], VALID_PASSWORD + current_time_year)
    expect(page).to have_content "Sign out"

    click_on "My profile"
    verify_user_is_on_profile_tab

    click_edit_on_password_section
    verify_password_section_expanded

    # Enter a valid password in New password and Re-enter new password fields
    fill_in_passwords(VALID_PASSWORD + current_time_year, VALID_PASSWORD + current_time_year + (1.to_s))

    verify_active_save_button

    click_inside_password_section("Save")
    wait_for_loaded
    verify_password_section_collapsed

    # The password change was successful
    click_edit_on_password_section
    signout
    login_as_user_and_go_to_aeb(VALID_LEARNER[:username], VALID_PASSWORD + current_time_year + (1.to_s))
    expect(page).to have_content "Sign out"

    click_on 'My profile'
    verify_user_is_on_profile_tab

    click_edit_on_password_section
    verify_password_section_expanded

    # Enter an invalid password in the Old password field
    fill_in_passwords(INVALID_PASSWORD + current_time_year, VALID_PASSWORD)
    verify_active_save_button
    click_inside_password_section("Save")

    within('.modal-dialog') { expect(page).to have_content "The password doesn't match our records. Please try again." }
    within('.modal-dialog') { find(:xpath, "//span[text()='Close']").click }
    verify_user_is_on_profile_tab

    # Enter a valid password in the Old password field
    # Enter an invalid password in the New password and Re-enter new password fields. Make sure the invalid passwords match.
    fill_in_passwords(VALID_PASSWORD + current_time_year + (1.to_s), INVALID_PASSWORD)
    expect(page).to have_xpath "//tr[td//div[text()='New password'] and td//label[text()='Password too short']]"
    verify_inactive_save_button

    # Change one of the invalid passwords so that the invalid passwords no longer match
    find(:css, "input[name='newPassword']").set(INVALID_PASSWORD + current_time_year)
    expect(page).to have_xpath "//tr[td//div[text()='Re-enter new password'] and td//label[text()='Passwords must match']]"

    # A warning should appear beside the New password field telling the user the password is too short
    find(:css, "input[name='newPassword']").set(INVALID_PASSWORD)
    find(:css, "input[name='confirmNewPassword']").set(INVALID_PASSWORD + current_time_year)
    expect(page).to have_xpath "//tr[td//div[text()='New password'] and td//label[text()='Password too short']]"
    verify_inactive_save_button

    # Change one of the invalid passwords to a valid password
    find(:css, "input[name='newPassword']").set(VALID_PASSWORD)
    expect(page).to have_xpath "//tr[td//div[text()='Re-enter new password'] and td//label[text()='Passwords must match']]"
    verify_inactive_save_button

    # Change the other invalid password, so both new passwords now match
    find(:css, "input[name='newPassword']").set(VALID_PASSWORD)
    find(:css, "input[name='confirmNewPassword']").set(VALID_PASSWORD)

    # The Save button is not active  (looks like a typo - save button should be active)
    verify_active_save_button
    click_inside_password_section("Save")
    wait_for_loaded
    verify_password_section_collapsed

    click_edit_on_password_section
    signout
    login_as_user_and_go_to_aeb(VALID_LEARNER)
    expect(page).to have_content "Sign out"
    signout
  end


  it 'allows learner to change cefr level and validate the change', :qa2only => true do

    verify_user_is_on_profile_tab

    change_lesson_level_and_verify_goals_are_present_in_cefr_levels
    verify_user_is_on_profile_tab
    verify_cefr_level_section_collpased

    click_edit_on_cefr_level_section
    verify_cefr_level_section_expanded

    cefr_level = find(:xpath, "//td[button[@class='cefr-radio-button selected']]//following-sibling::td[@ng-bind='cefrLevel.value']").text
    within('.cefr-level-edit') { click_button "Save" }

    # No pop-up appears Loading takes place and same lesson level is displayed
    wait_for_loaded
    # raise "Unexpected Popup" if page.has_css? ".modal-dialog"       #this fails as there is an unexpected popup

    if page.has_css?(".modal-dialog") # closing unexpected popup
      find("button[ng-if='!!cancelLabel'] > span").click
    end

    within('.cefr-level-view') { expect(page).to have_xpath "//div[@class='cefr-level-label ng-binding' and text()='#{cefr_level}']" }
    change_lesson_level_and_verify_goals_are_present_in_cefr_levels

    # click on Add new Goals
    @driver.find_element(:xpath, "//a[text()='Add new goals']").click
    expect(page).to have_css ".add-goals-view"
    signout
  end

  it 'allows learner to customize learner settings', :qa2only => true do

    verify_user_is_on_profile_tab

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    click_cancel_inside_learner_settings_section
    verify_learner_settings_section_collapsed

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    choose_language_and_time_zone(3, :cancel)

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    # selecting the voice type as we can't assert due to the non availability of the option selected tag
    find(:xpath, "(//div[@l10n='_edit_profile_voice_type'])[2]//following-sibling::select//option[1]").click

    choose_language_and_time_zone(2, :save)

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    choose_language_and_time_zone(1, :save)

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    # Click Edit on Any other section
    click_edit_on_cefr_level_section
    verify_learner_settings_section_collapsed
    verify_cefr_level_section_expanded

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    choose_language_and_time_zone(3, :edit)

    # no popup appears and the section user clicked Edit is opened
    expect(page).to_not have_css ".modal-dialog"
    verify_cefr_level_section_expanded

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    choose_language_and_time_zone(1, :save)

    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    # Resetting instruction language
    instruction_language = find(:xpath, "//div[@l10n='_edit_profile_site_display_language']/following-sibling::select//option[2]").text
    change_localization_language_to("English")

    # Save Changes
    click_save_inside_learner_settings_section
    verify_learner_settings_section_collapsed
    click_edit_on_learner_settings_section
    verify_learner_settings_section_expanded

    #resetting the values
    selected_values = [reset_learner_settings, instruction_language]

    click_save_inside_learner_settings_section
    verify_learner_settings_section_collapsed

    verify_selected_options_displayed_on_learner_settings_section(selected_values)

    click_edit_on_password_section
    signout
  end
end

