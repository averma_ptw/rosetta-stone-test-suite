require 'spec_helper'

#demo of visiting both AEB and Mixpanel in one trip must set TEST_ENV=qa2 and MIXPANEL=true in .env to run this test

describe 'localization language', type: :feature do
  before do
    login_as_user_and_go_to_aeb(VALID_COACH)
  end


  it 'can be changed to german and back to english', :feature => 'localization' do
    assert_localization_works
  end
end

describe 'mixpanel', type: :feature do
  before do
    visit 'https://mixpanel.com/login/'
    fill_in('id_email', :with => SeleniumHelper::Environment.mixpanel_name)
    fill_in('id_password', :with => SeleniumHelper::Environment.mixpanel_pwd)
    click_on 'Log in'
  end

  it 'has the userProfileUpdate event after localization language is changed', :feature => 'mixpanel' do
    visit "https://mixpanel.com/report/263732/live/#segfilter:(filters:!((filter:(operand:userProfileUpdate,operator:in),property:eventType,selected_property_type:string,type:string),(filter:(operand:'test_aria_coach@livemocha.com',operator:in),property:'$email',selected_property_type:string,type:string)),op:and)"
    expect(page).to have_content 'userProfileUpdate'
  end
end
