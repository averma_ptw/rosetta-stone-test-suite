# Installation

This project requires `Ruby` (2.0.0-p195). You can check out and install the project follows:

```
git checkout git@bitbucket.org:livemocha/aeb-qa.git
cd aeb-qa
bundle install
```

See [the wiki](https://jira.trstone.com/wiki/display/REQ/Acceptance+Testing) for more detailed instructions

# Resources
Here are some resources that may be useful:

*  [QA acceptance-testing wiki](https://jira.trstone.com/wiki/display/REQ/Acceptance+Testing)
*  [Selenium helper gem](https://bitbucket.org/livemocha/selenium-helper): provides many of the test-helper methods used by this project
*  [Capybara cheat sheet](http://www.launchacademy.com/codecabulary/learn-test-driven-development/rspec/capybara-cheat-sheet): list of Capybara methods and assertions

# Usage Instructions

To run tests in a single file, point your bundled `rspec` at the test as follows:

```
bundle exec rspec spec/features/my_test_file_here.rb
```

By default `rspec` will run a test using a browser on your computer and pointed at a local copy of the AEB and Launchpad applications. You can override these defaults using the following parameters:

*  **TEST_DRIVER**: Where should the test be run- locally or remotely? Acceptable values are: *selenium* (default, run locally), *browser_stack* (run remotely)
*  **TEST_ENV**: Which environment should the tests be run against? Acceptable values are: *local* (default), *dev*, *qa2*, *prod*
*  **MIXPANEL**: Are we running tests that go to mixpanel.com? If so, and if using the TEST_DRIVER "browser_stack," this will allow browser_stack to tunnel there successfully. Acceptable values are: *false* (default), *true*

### Sample Test Invocation Commands

Here's how to run tests:

```
    # Suites of multiple specs/files (see also scripts/suites)
    TEST_ENV=prod bundle exec rspec --tag prodonly # from prod environment, just run tests with tag "prodonly"
    TEST_ENV=dev scripts/suites/cross_env # run the cross env suite against dev
    TEST_ENV=qa2 scripts/suites/cross_env # run the cross env suite against qa2

    # Running just the specs in one file
    TEST_ENV=prod rspec spec/features/first_time_experience_spec.rb

    # Running just the spec on a certain line (line 10, in this case)
    TEST_ENV=prod rspec spec/features/first_time_experience_spec.rb:10
```
