# Updating this gem
 * Commit to develop
 * Push develop to origin

# Using this gem
 * Reference this gem, selenium-helper, in your app's Gemfile [like we do in AEB](https://bitbucket.org/livemocha/advanced-english/src/HEAD/capybara/Gemfile?at=develop)
 * In your app, run "bundle install" to get new versions of selenium-helper

# Avoiding passing environment variables on the command line

If you find yourself using the same environment variables all the time on the command line, you can instead make a .env file at the root of your project (make sure to .gitignore it), and fill it with contents like this:

    TEST_ENV=dev
    TEST_DRIVER=selenium

# Using the hot console test harness

Run the following from your test project:

    bundle exec console_test_harness.rb

It will fire up firefox and send you to the launchpad sign in page. Once the REPL starts up, you should have access to most everything that your tests have access to. Try the following:

    login('sxu+LEARNER@rosettastone.com', 'password')
