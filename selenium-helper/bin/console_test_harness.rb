#!/usr/bin/env ruby

require 'rspec'
require 'selenium-webdriver'
require 'selenium-helper'
require 'pry'
include RSpec::Expectations
include SeleniumHelper::Actions
include SeleniumHelper::Utilities
include RSpec::Matchers

capy_enabled = true if ENV['USE_CAPYBARA'] == 'true'

if capy_enabled
  puts "Loading Capybara"
  require 'capybara/rspec'
  include Capybara::DSL
end

begin
  require File.expand_path('spec/spec_helper')
rescue LoadError
  STDERR.puts "ERROR: The console_test_harness.rb script requires you to put your project-specific test dependencies in a file called spec/spec_helper.rb. Add that file!"
  raise
end


browser = ENV['TEST_BROWSER']
browser = 'firefox' if !['ie', 'firefox', 'chrome', 'safari'].include?(browser)

if capy_enabled
  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => browser.to_sym)
  end
  Capybara.current_driver = :selenium
  @driver = page.driver.browser
else
  @driver = Selenium::WebDriver.for browser.to_sym
end

@driver.manage.timeouts.implicit_wait = SeleniumHelper::Settings::DEFAULT_WAIT
@wait = Selenium::WebDriver::Wait.new(:timeout => SeleniumHelper::Settings::DEFAULT_WAIT)

# some lauchpad-specific stuff that seems harmless
@driver.get SeleniumHelper::Settings.launchpad_url
@verification_errors = []

# start up the REPL, baby
binding.pry

Kernel.at_exit do
  @driver.quit
end