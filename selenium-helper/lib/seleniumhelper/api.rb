require 'rubygems'
require "net/https"
require 'uri'
require 'cgi'
require 'yaml'
require 'json'
require 'rspec' # TODO Does this need to be here?
require 'rspec/expectations' # TODO Does this need to be here?
require 'openssl'
require 'base64'
include RSpec::Expectations # TODO Does this need to be here?


def generate_proto_user_request(options={})
  email_address = "#{(options[:base_email] || 'testlab-gg')}+#{Time.now.to_i}@rosettastone.com"
  target_language = {"language" => (options[:targetLanguage] || "en-US")}
  if options[:level]
    target_language.merge!("level" => options[:level])
  end
  if options[:how_level_was_chosen]
    target_language.merge!("howChosen" => options[:how_level_was_chosen])
  end
  
  ({"type"=>"ProtoUser",
              "roles"=> (options[:roles] || ["role.AriaLearner"]).to_a, #role.AriaTutoringStudent , role.OrganizationAdmin
              "organization"=> options[:organization] || SeleniumHelper::Settings.environment_settings["organization"], 
              "firstName"=> options[:firstName] || random_string, 
              "lastName"=> options[:lastName] || random_string, 
              "email"=> email_address, 
              "localizationLanguage"=> options[:localizationLanguage] || "en-US",
              "targetLanguage"=> target_language
            }).to_json
end

def register_user_request(options={}) 
  # This method should usually be used in conjunction with the create_proto_user_with_api method.
  # Required options parameters: :_id (the id of a valid proto user document.)
  # Optional options parameters: :localizationLanguage, :voiceType, :password, :userField4, :originLanguage, :timezone.
  
  # You need an auth token for this.
  token = options[:token] || generate_auth_token
  
  # Get the proto_user doc
  proto_user_doc = rest_get(SeleniumHelper::Settings.environment_settings["scholar_url"] + "/proto/user/" + options[:_id] + "/register", {"Authorization" => "Bearer #{token}"})
  
  (proto_user_doc.merge({             
              "localizationLanguage" => options[:localizationLanguage] || proto_user_doc["localizationLanguage"],
              "preferences" => {"voiceType" => options[:voiceType] || "male" },
              "password" => options[:password] || SeleniumHelper::Settings.environment_settings["valid_password"],
              "userField4" => options[:userField4] || true,
              "originLanguage" => options[:originLanguage] || "en-US",
              "timezone" => options[:timezone] || "America/New_York"
            })).to_json
end

def create_org_request(options={})
  # This method should usually be used in conjunction with the create_org_with_api method.
  default_name = "test-org-#{Time.now.to_i}"
  
  ({"adminRole" => "role.OrganizationAdmin",
    "admins" => [],
    "disableBeans" => false,
    "type" => "Organization",
    "seats" => 0,
    "active" => true,
    "configuration" =>
      {"useSalesforceForAriaSupport" => true,
       "allowSearchingByActivitySetIdInEditor" => true,
       "showAriaInterface" => true,
       "showAddGoalsInterface" => true},
    "name" => options[:org_name] || default_name,
    "namespace" => options[:org_namespace] || default_name}).to_json
end
  
def rest_get(request_url, request_headers = {})
  uri = URI.parse("#{request_url}")
  request = Net::HTTP::Get.new(uri.request_uri)
  http_headers(request, request_headers)
  response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request request}
  response_hash = JSON.parse(response.body, :symbolize_names => true)
  return response_hash
end
  
def rest_post(request_url, request_body = nil, request_headers = {})
  uri = URI.parse(request_url)
  request = Net::HTTP::Post.new(uri.request_uri)
  http_headers(request, request_headers)
  request.body = request_body if request_body
  response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request request}
  response_hash = JSON.parse(response.body, :symbolize_names => true)
  return response_hash
end
  
def rest_put(request_url, request_body = nil, request_headers = {})
  uri = URI.parse("#{request_url}")
  request = Net::HTTP::Put.new(uri.request_uri)
  http_headers(request, request_headers)
  request.body = request_body if request_body
  response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request request}
  response_hash = JSON.parse(response.body, :symbolize_names => true)
  return response_hash
end
  
def rest_delete(request_url, request_headers = {})
  uri = URI.parse("#{request_url}")
  request = Net::HTTP::Delete.new(uri.request_uri)
  http_headers(request, request_headers)
  response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request request}
  response_hash = JSON.parse(response.body, :symbolize_names => true)
  return response_hash
end

def http_headers(request, headers) # helper used by the above methods to parse headers.
  if headers && headers.is_a?(Hash)
    headers.each { |key, value| request[key] = value }
  end
  return request
end
