module SeleniumHelper
  module Aeb
    def aria_elements(element, options={})
      # Process any setup options
      set_implicit_wait(options[:implicit_wait]) if options[:implicit_wait]
      
      # Process find_element request
      results = case element
        when :live_tutoring_session
          @driver.find_element(:css, "a[l10n='_coaching_sessions']") 
          #class changes name if option is disabled to sc-button coaching-sessions allow-select sc-hidden
        when :add_new_goals
          @driver.find_element(:css, "a[l10n='_add_goals']")
        when :sign_out
          @driver.find_element(:css, "a[l10n='_sign_out']")
      end
      
      rescue Selenium::WebDriver::Error::NoSuchElementError
      results = false

      # Process any teardown options
      set_implicit_wait(DEFAULT_WAIT) if options[:implicit_wait]
      
      # Return results
      return results
    end
    
      
    def aria_goals_screen_elements(element, options={})
      set_implicit_wait(options[:implicit_wait]) if options[:implicit_wait]
      
      # Process find_element request
      results = case element
        when :level_score # This is the score that appears on the Goal Selection screen (first launch)
          @driver.find_element(:css, "div[class='level-name ng-binding']")
        when :goals_array
          @driver.find_elements(:css, "li[ng-bind='goal.title']")          
        when :continue_button
          @driver.find_element(:css, "button[l10n='_continue']")
      end
          
      rescue Selenium::WebDriver::Error::NoSuchElementError
      results = false

      # Process any teardown options
      set_implicit_wait(DEFAULT_WAIT) if options[:implicit_wait]
  
      # Return results
      return results
    end
    
    def aeb_navigation_elements(element, options={})
      set_implicit_wait(options[:implicit_wait]) if options[:implicit_wait]
      
#      results = case element
#        # TODO        
#      end

      rescue Selenium::WebDriver::Error::NoSuchElementError
      results = false

      # Process any teardown options
      set_implicit_wait(DEFAULT_WAIT) if options[:implicit_wait]

      # Return results
      return results      
    end
    
    def aeb_my_goals_tab_elements(element, options={})
      set_implicit_wait(options[:implicit_wait]) if options[:implicit_wait]
      
      results = case element
        when :lesson_level # This is the level that appears on the My Goals screen (2nd+ launch)
          @driver.find_element(:css, "span[class='cefr-level ng-binding']")
        when :selected_goals_array
          @driver.find_elements(:css, "li[class='goal-lesson-row ng-scope']")
      end

      rescue Selenium::WebDriver::Error::NoSuchElementError
      results = false

      # Process any teardown options
      set_implicit_wait(DEFAULT_WAIT) if options[:implicit_wait]

      # Return results
      return results
    end
  end
end
