require 'seleniumhelper/environment'
require 'seleniumhelper/settings'

module SeleniumHelper
  module Utilities
    def generate_auth_token(options={}) # Returns an auth token for the specified user (or default)
      auth_data = URI.encode_www_form({:grant_type => "password", 
                                       :username => (options[:username] || SeleniumHelper::Settings.environment_settings["valid_orgadmin"][:username]), 
                                       :password => (options[:password] || SeleniumHelper::Settings.environment_settings["valid_orgadmin"][:password]), 
                                       :client_id => (options[:client] || "client.siren"), # TODO - Consider using client.rsm as default (if that's what is being done on LP/AT signin)
                                       :client_secret => ""})
      response = rest_post(SeleniumHelper::Settings.environment_settings["tully_url"]+"/oauth/token", auth_data, {"Content-Type" => "application/x-www-form-urlencoded"})
      return response[:access_token] rescue nil
    end    

    def random_string(length=5)
      return Array.new(length){(rand(122-97)+97).chr}.join
    end

    def scholar_url
      SeleniumHelper::Settings.environment_settings["scholar_url"]
    end

    def get_user_doc(user_id, options = {})
      token = options[:token] || generate_auth_token
      endpoint = "/users/" + user_id
      user = rest_get(scholar_url + endpoint, {"Authorization" => "Bearer #{token}"})
      
      # Try proto user endpoint if user endpoint fails
      if user[:error]
        endpoint = "/proto/user/" + user_id + "/register"
        user = rest_get(scholar_url + endpoint, {"Authorization" => "Bearer #{token}"})
      end

      return user
    end

    def create_proto_user_with_api(options={})
      token = options[:token] || generate_auth_token
      request = generate_proto_user_request(options)
      user = rest_post(scholar_url + "/proto/user", request, {"Authorization" => "Bearer #{token}"})
      
      # Add the new user to the admins array if there is an admin role
      admin_roles = ["role.OrganizationAdmin"] # This array will allow additional admin roles to be added to this method if required.
      if options[:roles] and options[:roles].all? {|e| admin_roles.include?(e)}
        endpoint = "/admin/" + user[:organization] + "/add_admin"
        request = ({"user" => user[:_id]}).to_json
        rest_post(scholar_url + endpoint, request, {"Authorization" => "Bearer #{token}", "Content-Type" => "application/json;charset=utf-8"})
      end
      
      return user
    end
    
    def register_user_with_api(options={})
      request = register_user_request(options)
      endpoint = "/proto/user/" + options[:_id] + "/register"
      proto_user = rest_post(scholar_url + endpoint, request)
      user = get_user_doc(proto_user[:userID], options)
      return user
    end

    def create_proto_user(params={})
      params[:token] = generate_auth_token(params)
      proto_user = create_proto_user_with_api(params)
      if params[:register] # Registers above proto user and returns registered user doc
        user = register_user_with_api(proto_user.merge(params)).merge({:password => params[:password], :token => params[:token]})
        return user
      else
        return proto_user
      end
    end
    
    def create_org_with_api(options={})
      if SeleniumHelper::Environment.prod?
        puts "WARNING:  You are creating a test org in production!  You have 5 seconds to command+c"
        5.times do |w|
          puts "#{5-w}..."
          sleep(1)
        end
        puts "Hope you know what you're doing!"
      end
      
      token = generate_auth_token(options.merge(VALID_SUPERORGADMIN))
      endpoint = "/organization"
      request = create_org_request(options)
      rest_post(scholar_url + endpoint, request, {"Authorization" => "Bearer #{token}", "Content-Type" => "application/json;charset=UTF-8"})
    end
  end
end