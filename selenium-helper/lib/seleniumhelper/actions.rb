require 'seleniumhelper/launchpad'
require 'seleniumhelper/aeb'

module SeleniumHelper
  module Actions
    include SeleniumHelper::Launchpad
    include SeleniumHelper::Aeb

    def login(params={})
      credentials = {:username => params[:username] || params[:email] || VALID_LEARNER[:username], 
                     :password => params[:password] || VALID_LEARNER[:password]}

      wait.until { launch_pad_elements(:username_field) }
      launch_pad_elements(:username_field).send_keys credentials[:username]
      launch_pad_elements(:password_field).send_keys credentials[:password]
      launch_pad_elements(:sign_in_button).click
    end

    def signout(from=nil)
      if from == nil
        if @driver.current_url.include? SeleniumHelper::Settings.environment_settings['lp_url']
          from = :launchpad
        elsif @driver.current_url.include? SeleniumHelper::Settings.environment_settings['at_url']
          from = :admin_tool
        elsif @driver.current_url.include? SeleniumHelper::Settings.environment_settings['aeb_url']
          from = :aeb
        else
          puts "I don't know how to log out from here."
        end
      end
      
      case from
        when :launchpad
          launch_pad_elements(:show_menu_button).click
          launch_pad_elements(:logout_menu_option).click
          assert_signed_out_from_launchpad
        when :admin_tool
          admin_navigation_elements(:username_menu).click
          admin_navigation_elements(:signout_button).click
          assert_signed_out_from_launchpad
        when :aeb
          aria_elements(:sign_out).click
          assert_signed_out_from_launchpad            
      end
    end

    def assert_signed_out_from_launchpad
      wait.until { launch_pad_elements(:username_field) }
    end

    def switch_products(from)
      case from
        when :admin_tool
          admin_navigation_elements(:username_menu).click
          admin_navigation_elements(:switch_products_button).click
          wait.until { launch_pad_elements(:launch_content_div) }
        when :aeb
          aria_elements(:exit).click
          wait.until { launch_pad_elements(:launch_content_div) }
      end
    end

    def wait
      @wait ||= Selenium::WebDriver::Wait.new(:timeout => Settings::DEFAULT_WAIT, :driver => @driver)
    end
  end
end
