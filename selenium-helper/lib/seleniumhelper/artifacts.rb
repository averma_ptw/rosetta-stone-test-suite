# Includes constants that are shorthand for items accessible through Settings

## URLs
SCHOLAR_URL = SeleniumHelper::Settings.environment_settings["scholar_url"]
TULLY_URL = SeleniumHelper::Settings.environment_settings["tully_url"]

## Users
VALID_USER_WITH_ALL_ROLES = SeleniumHelper::Settings.environment_settings["valid_user_with_all_roles"]
USER_WITH_NO_ROLES = SeleniumHelper::Settings.environment_settings["user_with_no_roles"]
VALID_ORGADMIN = SeleniumHelper::Settings.environment_settings["valid_orgadmin"]
VALID_ARIA_ADMIN = SeleniumHelper::Settings.environment_settings["valid_aria_admin"]
VALID_SUPERORGADMIN = SeleniumHelper::Settings.environment_settings["valid_superorgadmin"]
VALID_CLIENTSERVICESMANAGER = SeleniumHelper::Settings.environment_settings["valid_clientservicesmanager"]
VALID_ORDERMANAGER = SeleniumHelper::Settings.environment_settings["valid_ordermanager"]
VALID_LEARNER = SeleniumHelper::Settings.environment_settings["valid_learner"]
VALID_COACH_LEARNER = SeleniumHelper::Settings.environment_settings["valid_tutoring_learner"]
VALID_GROUP_LEARNER = SeleniumHelper::Settings.environment_settings["valid_grouptutoring_learner"]
VALID_COACH = SeleniumHelper::Settings.environment_settings["valid_coach"]
VALID_AUTHOR = SeleniumHelper::Settings.environment_settings["valid_author"]
USER_WITH_INCORRECT_USERNAME = SeleniumHelper::Settings.environment_settings["user_with_incorrect_username"]
USER_WITH_INCORRECT_PASSWORD = SeleniumHelper::Settings.environment_settings["user_with_incorrect_password"]

## Other
ORGANIZATION = SeleniumHelper::Settings.environment_settings["organization"]
VALID_PASSWORD = SeleniumHelper::Settings.environment_settings["valid_password"]
INVALID_PASSWORD = SeleniumHelper::Settings.environment_settings["invalid_password"]