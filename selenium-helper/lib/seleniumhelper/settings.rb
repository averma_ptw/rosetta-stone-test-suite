require 'yaml'
require 'seleniumhelper/environment'

module SeleniumHelper
  module Settings
    DEFAULT_WAIT = 10
    class << self
      def all_settings
        @config ||= YAML.load_file(File.join(File.dirname(__FILE__), '..', '..', 'config', 'settings.defaults.yml'))
      end

      def environment_settings
        env = SeleniumHelper::Environment.name
        settings = all_settings[env]
        raise "no settings for environment: #{env.inspect}" if settings.nil?
        
        if SeleniumHelper::Environment.name == "prod"
          settings['valid_aria_admin'] = {:username => "aria_test_all@rosettastone.com", :password => "asdfgh"}
          settings['valid_superorgadmin'] = {:username => ENV['SOA_USERNAME'], :password => ENV['SOA_PASSWORD']}
          settings['valid_clientservicesmanager'] = {:username => ENV['CSM_USERNAME'], :password => ENV['CSM_PASSWORD']}
          settings['valid_ordermanager'] = {:username => ENV['OM_USERNAME'], :password => ENV['OM_PASSWORD']}
        end
        
        settings
      end

      def aeb_url
        environment_settings['aeb_url']
      end

      def launchpad_url
        environment_settings['launchpad_url']
      end

      def valid_learner
        {:username => 'testlab-gg+learner@rosettastone.com', :password => 'password'}
      end
      
      def placeholder_org_name
        environment_settings['placeholder_org_name']
      end
    end
  end
end

