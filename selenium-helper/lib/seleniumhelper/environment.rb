require 'yaml'

module SeleniumHelper
  module Environment
    class << self
      def name
        if ENV['PROD'] == 'true' # backwards compat. remove when everyone's using TEST_ENV
          'prod'
        elsif ENV['TEST_ENV']
          ENV['TEST_ENV'].downcase
        else
          'local'
        end
      end

      #TEST_ENV can be set to "mixpanel" if that is the only site used in test, otherwise set this flag to use mixpanel in addition to qa2/prod/local
      def use_mixpanel?
        return ENV['MIXPANEL'] == 'true'
      end

      def backend_environment
        if name == 'local'
          'dev' # let's use scholar on dev when using "local" for now, until we make another env variable
        else
          name
        end
      end

      def needs_to_tunnel_to_intranet?
        browser_stack? && intranet?
      end

      def browser_stack?
        ENV['TEST_DRIVER'] == 'browser_stack'
      end

      def intranet?
        local? || qa? || dev?
      end

      def local?
        name == 'local'
      end

      def prod?
        name == 'prod'
      end

      def qa2?
        name == 'qa2'
      end

      # non-localhost, non-production environments
      def qa?
        name.start_with?('qa') || dev?
      end

      def dev?
        name == 'dev'
      end

      def mixpanel?
        name == 'mixpanel'
      end

      def mixpanel_name
        ENV['MIXPANEL_NAME']
      end

      def mixpanel_pwd
        ENV['MIXPANEL_PWD']
      end
    end
  end
end

