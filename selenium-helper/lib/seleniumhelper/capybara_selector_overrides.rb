# this won't be loaded by default when you use the selenium-helper gem. to load it:
# require 'selenium-helper/capybara_selector_overrides'

if defined?('Capybara')
  module AEB
    module XPath
      module HTML
        include ::XPath::DSL::TopLevel
        extend self

        # sproutcore stuff that no one cares about any more
        def sc_button(locator)
          locator = locator.to_s
          descendant(:*)[attr(:class).contains('sc-button')][string.n.is(locator)]
        end

        # hot angular ng-click handler
        def ng_clickable_link(locator)
          locator = locator.to_s
          descendant(:a)[attr(:'ng-click')][string.n.is(locator)]
        end

        # matches if passed goal (course) title from my goals page
        def my_goals_course_row(locator)
          locator = locator.to_s
          descendant(:div)[attr(:'ng-click')][string.n.is(locator)]
        end

      end
    end
  end

  puts('Monkeypatching link_or_button Capybara finder/selector. Affects click_on and click_link_or_button.') if ENV['TEST_DEBUG']
  # This monkeypatches the link_or_button selector to match Sproutcore elements
  # that behave like links. Calls `Capybara.add_selector` to replace the existing
  # link_or_button selector. Base implementation borrowed from Capybara 2.2.1.
  Capybara.add_selector(:link_or_button) do
    label "link or button"
    xpath do |locator|
      XPath::HTML.link_or_button(locator) + AEB::XPath::HTML.sc_button(locator) + AEB::XPath::HTML.my_goals_course_row(locator) + AEB::XPath::HTML.ng_clickable_link(locator)
    end
    filter(:disabled, :default => false) { |node, value| node.tag_name == "a" or not(value ^ node.disabled?) }
  end
end
