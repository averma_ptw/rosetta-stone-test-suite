module SeleniumHelper
  module Launchpad
    def launch_pad_elements(element, options={})
      # Process any setup options
      set_implicit_wait(options[:implicit_wait]) if options[:implicit_wait]
      
      # Process find_element request
      results = case element
      ## Login form ##
        when :username_field
          @driver.find_element(:id => 'email')
        when :password_field
          @driver.find_element(:name, "password")  # TODO - Get an ID added to this.
        when :sign_in_button
          @driver.find_element(:css, "button")  # TODO - Get an ID added to this.
          ## Logged in form aka LaunchPad homepage ##
        when :launch_content_div
          @driver.find_element(:css, ".launch-content") 
        when :show_menu_button
          @driver.find_element(:css => ".header-menu") # TODO - Get an ID added to this.
        when :logout_menu_option
          @driver.find_element(:id, "signout")
        when :aria_button
          @driver.find_element(:id => 'launch_aria') 
      end  
      
      rescue Selenium::WebDriver::Error::NoSuchElementError
      results = false

      # Process any teardown options
      set_implicit_wait(DEFAULT_WAIT) if options[:implicit_wait]
      
      # Return results
      return results
    end

  end
end

