# this won't be loaded by default when you use the selenium-helper gem. to load it:
# require 'selenium-helper/browserstack'

# we don't have a rack server to run locally
Capybara.run_server = false

driver = (SeleniumHelper::Environment.browser_stack?) ? :browser_stack : :selenium
url = "https://brianvaughn:p46Gbg4gJpByypWF2o8w@hub.browserstack.com/wd/hub"
capabilities = Selenium::WebDriver::Remote::Capabilities.new
capabilities_options = {}
capabilities_options['browser'] = ENV['TEST_BROWSER'] || 'Chrome'
capabilities_options['browser_version'] = ENV['TEST_BROWSER_VERSION'] || '32'
capabilities_options['os'] = ENV['TEST_OS'] || 'Windows'
capabilities_options['os_version'] = ENV['TEST_OS_VERSION'] || '7'
capabilities_options['browserstack.tunnel'] = 'true' if SeleniumHelper::Environment.needs_to_tunnel_to_intranet?
capabilities_options['browserstack.debug'] = 'true'
capabilities_options['acceptSslCerts'] = 'true'
puts "Selenium::WebDriver::Remote::Capabilities: #{capabilities_options.inspect}" if ENV['TEST_DEBUG']
capabilities.merge!(capabilities_options)

Capybara.register_driver :browser_stack do |app|
  Capybara::Selenium::Driver.new(
    app,
    :browser => :remote,
    :url => url,
    :desired_capabilities => capabilities)
end

Capybara.current_driver = driver
Capybara.javascript_driver = driver
Capybara.default_driver = driver

BROWSERSTACK_CONNECT_TIMEOUT = 10

# Make sure you use should_not for negative asserts (make sure this page doesn't say "sausage"), rather than !should have
# I couldn't get saucelabs to actually work :(
# require File.expand_path('../sauce_helper', __FILE__)
# Capybara.current_driver = :sauce
# Capybara.javascript_driver = :sauce
# Capybara.default_driver = :sauce

#if testing on qa, meaning browsterstack tunneling and sproutcore, bump wait time *way* up
Capybara.default_wait_time = (SeleniumHelper::Environment.needs_to_tunnel_to_intranet?) ? 180 : 30

if SeleniumHelper::Environment.needs_to_tunnel_to_intranet?
  RSpec.configure do |config|

    config.before(:suite) do
      tunnel_base = File.expand_path('../../', __FILE__)
      tunnel_location = File.join(tunnel_base, 'BrowserStackLocal')

      if !File.exists?(tunnel_location)
        puts "Could not find BrowserStack tunnel executable."
        require 'net/http'
        if RUBY_PLATFORM =~ /darwin/
          uri = 'http://www.browserstack.com/browserstack-local/BrowserStackLocal-darwin-x64.zip'
        elsif RUBY_PLATFORM =~ /linux/
          if RUBY_PLATFORM =~ /64/
            uri = 'http://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-x64.zip'
          else
            uri = 'http://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-ia32.zip'
          end
        else
          raise "Unexpected platform #{RUBY_PLATFORM}. Expected darwin or linux."
        end
        puts "Downloading from #{uri}"
        zip_file = '/tmp/BrowserStackTunnel.zip'
        File.open(zip_file, 'wb') do |f|
          f.write Net::HTTP.get(URI.parse uri)
        end
        puts "Unzipping tunnel to #{tunnel_base}"
        Kernel.system("unzip #{zip_file} -d #{tunnel_base}")
        if !File.exists?(tunnel_location)
          raise "Still could not find tunnel at #{tunnel_location}. Aborting!"
        else
          puts "Unzipped tunnel to #{tunnel_location}"
        end
      end

      tunnel_val = "#{tunnel_location} -v p46Gbg4gJpByypWF2o8w "
      if SeleniumHelper::Environment.qa?  #any QA server
        qa_name = SeleniumHelper::Environment.name
        base_url = qa_name + ".dev.rosettastone.com"
        aeb_prefix = 'advancedenglish'
        aeb_tunnel = aeb_prefix + "-" + base_url+ ",443,1"
        launchpad_tunnel = "login-" + base_url + ",443,1"
        tully_tunnel = "tully-" + base_url + ",443,1"
        scholar_tunnel = "scholar-" + base_url + ",443,1"
        biblio_tunnel = "biblio-" + base_url + ",443,1"
        tunnels = [aeb_tunnel, launchpad_tunnel, tully_tunnel, scholar_tunnel, biblio_tunnel].join(',')
        tunnel_val = tunnel_val + tunnels
      elsif SeleniumHelper::Environment.mixpanel?  #when mixpanel *only*
        tunnel_val = tunnel_val + "mixpanel.com,80,0"
        puts tunnel_val
      else #default is local
        tunnel_val = tunnel_val + "scholar-dev.dev.rosettastone.com,443,1,biblio-dev.dev.rosettastone.com,443,1,localhost,4020,0"
        puts tunnel_val
      end

      if SeleniumHelper::Environment.use_mixpanel? #when using mixpanel in addition to local or qa
        tunnel_val = tunnel_val + ",mixpanel.com,80,0"
      end

      puts "Opening tunnel: " + tunnel_val

      pipe_me_in, pipe_peer_out = IO.pipe

      $browser_stack_tunnel_pid = Kernel.fork do
        STDOUT.reopen(pipe_peer_out)
        Kernel.exec tunnel_val
      end

      puts "Waiting for browserstack tunneled connection"
      starting_time = Time.now

      while true
        if Time.now - starting_time > BROWSERSTACK_CONNECT_TIMEOUT
          raise "Could not connect to browserstrack after waiting #{BROWSERSTACK_CONNECT_TIMEOUT} seconds"
        end
        output = pipe_me_in.gets
        puts output
        if output.include?('Connected.')
          break
        end
      end

      puts "Started BrowserStack tunnel at pid #{$browser_stack_tunnel_pid}"
    end

    config.after(:suite) do
      puts "Killing BrowserStack tunnel at pid #{$browser_stack_tunnel_pid}"
      Process.kill('SIGINT', $browser_stack_tunnel_pid)
    end
  end
end
