Gem::Specification.new do |s|
  s.name        = 'selenium-helper'
  s.version     = '0.0.1'
  s.required_ruby_version = '>= 1.9.3'
  s.authors = ['Rosetta Stone']
  s.platform = Gem::Platform::RUBY
  s.summary     = "Selenium test helper"
  s.description = "Libraries useful for people writing acceptance tests in ruby (and selenium-webdriver)"

  # this lists only lib files, but really since we never actually package this
  # gem (we only ever use it from source), the files property is never used.
  s.files = Dir['./lib/**/*.rb']
  s.executables   = ["console_test_harness.rb"]
  s.require_paths = ['lib']

  s.add_dependency 'dotenv'
  s.add_dependency 'pry', '=0.9.12.6' # for console_test_harness.rb
end
